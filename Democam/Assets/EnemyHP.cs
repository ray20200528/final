﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHP : MonoBehaviour
{
    public float maxhp;
    public float hp;
    
    // Start is called before the first frame update
    void Start()
    {
        hp = maxhp;
    }



    void OnTriggerEnter(Collider DamageEvent)
    {
        if (DamageEvent.tag == "PlayerAttack")
        {

            HittedMatEffect sc = gameObject.GetComponent<HittedMatEffect>();
            if (null == sc)
                sc = gameObject.AddComponent<HittedMatEffect>();
            sc.Active();
            sc.SetColor(Color.white);
            //控制被擊中時的閃光特效
        }
        
    }
     
}
