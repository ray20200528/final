﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sniper : MonoBehaviour
{
    enum FSM
    {
        None = -1,
        wander,
        left,
        shoot,
        right,
        Idle,
        win,
        hurt
    }
    public GameObject target;                //目标角色         
    private Animator ani;
    private FSM state;
    private float Idletime;
    private float currenttime;
    public int r;
    public float rotspeed;
    public GameObject bullet;
    public float Timer;
    public GameObject gun;
    public GameObject bomb;
    public float bombtime;
    public AudioClip hurt;
    AudioSource ads;
    EnemyHP enemyhp;
    CharacterController chr;
    public GameObject exp;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindWithTag("Player");
        ani = GetComponent<Animator>();
        state = FSM.Idle;
        currenttime = 0.0f;
        Idletime = Random.Range(2, 3);
        Timer = 1.5f;
        bombtime = 2f;
        enemyhp = GetComponent<EnemyHP>();
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        ads = GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        Vector3 md = target.transform.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(md);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
        if (chr.PlayerHp>0)
        {
            if (chr.power != true)
            {
                if (state == FSM.wander)
                {
                    ani.SetBool("boom",true);

                    if (currenttime > Idletime)
                    {
                        ani.SetBool("boom", false);
                        currenttime = 0.0f;
                        r = Random.Range(0, 2);
                        switch (r)
                        {
                            case 0:
                                state = FSM.left;
                                Idletime = Random.Range(2, 3);
                                break;
                            case 1:
                                state = FSM.right;
                                Idletime = Random.Range(2, 3);
                                break;
                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.shoot)
                {
                    ani.SetBool("attack", true);

                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        ani.SetBool("attack", false);
                        r = Random.Range(0, 4);
                        switch (r)
                        {
                            case 0:
                                state = FSM.left;
                                break;
                            case 1:
                                state = FSM.right;
                                break;
                            case 2:
                                state = FSM.Idle;
                                break;
                            case 3:
                                state = FSM.wander;
                                Idletime = Random.Range(1, 2);
                                break;
                        }

                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.left)
                {
                    transform.Translate(10 * Time.deltaTime, 0, 0);
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;

                        r = Random.Range(0, 2);
                        switch (r)
                        {
                            case 0:
                                state = FSM.shoot;
                                break;
                            case 1:
                                state = FSM.wander;
                                Idletime = Random.Range(1, 2);
                                break;

                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.right)
                {
                    transform.Translate(-10 * Time.deltaTime, 0, 0);
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        r = Random.Range(0, 2);
                        switch (r)
                        {
                            case 0:
                                state = FSM.shoot;
                                break;
                            case 1:
                                state = FSM.wander;
                                Idletime = Random.Range(1, 2);
                                break;

                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.Idle)
                {
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;

                        state = FSM.shoot;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
            }else
            {
                state = FSM.hurt;
                ani.SetBool("attack", false);
                ani.SetBool("boom", false);
            }
        }
        else
        {
            state = FSM.win;
        }
        if (state == FSM.win)
        {
            ani.SetBool("vic", true);
            if(chr.PlayerHp>0)
            {
                state = FSM.wander;
                ani.SetBool("vic", false);
            }
        }
        if(state == FSM.hurt)
        {
            if (chr.power)
            {
                state = FSM.Idle;
            }
        }
        if (enemyhp.hp <= 0)
        {
            ani.SetBool("dead", true);
            //Invoke("Dead", 0.5f);
            //Destroy(Instantiate(exp, transform.position, Quaternion.identity), 1);
            //this.gameObject.SetActive(false);
        }

    }
    
    void bombfire()
    {
        Instantiate(bomb, transform.position, transform.rotation);
    }
    void gunfire()
    {
        Instantiate(bullet, gun.transform.position, gun.transform.rotation);
        ads.PlayOneShot(hurt);
        //print("fire000");
    }
    private void OnDrawGizmos()
    {
        Debug.DrawLine(this.transform.position, this.transform.position + transform.forward * 20.0f, Color.red);
    }
    void Dead()
    {
        Destroy(Instantiate(exp, transform.position, Quaternion.identity), 0.8f);
        this.gameObject.SetActive(false);
        //Destroy(gameObject);
        //GameObject.Find("enemypool").GetComponent<enemypool>().Recovery(gameObject);
    }

}
