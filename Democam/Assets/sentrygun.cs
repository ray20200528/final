﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class sentrygun : MonoBehaviour
{
    public enum FSM
    {
        shoot,
        idle,
        hurt,
        win
    }
    public float Timer;
    private float idletime;
    public Transform player;
    private FSM state;
    public GameObject bullet;
    public float rotspeed;
    EnemyHP enemyhp;
    CharacterController chr;
    public GameObject exp;
    public AudioClip hurt;
    AudioSource ads;
    bigboss sir;
    //finalboss bigsir;
    GameObject fsir;
    // Start is called before the first frame update
    void Start()
    {
        ads = GetComponent<AudioSource>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        Timer = 0.0f;
        enemyhp = GetComponent<EnemyHP>();
        idletime = 2;
        state = FSM.idle;
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        fsir = GameObject.FindGameObjectWithTag("Bigboss");
        
        sir = GameObject.Find("bigboss").GetComponent<bigboss>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //bigsir = GameObject.Find("finalboss").GetComponent<finalboss>();
        
        Vector3 md = player.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(md);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
        if(sir != null)
        {
            
            if (sir.GetComponent<EnemyHP>().hp<=0)
            {
                Destroy(Instantiate(exp, transform.position, Quaternion.identity), 1);
                this.gameObject.SetActive(false);
            }
        }
        if (fsir != null)
        {
            
            if (fsir.GetComponent<BossHP>().hp <= 0)
            {
                Destroy(Instantiate(exp, transform.position, Quaternion.identity), 1);
                this.gameObject.SetActive(false);
            }
        }
        Timer += Time.deltaTime;
        if (chr.PlayerHp > 0)
        {
            
            if (chr.power != true)
            {
                if (state == FSM.idle)
                {
                    if (Timer > idletime)
                    {
                        
                        
                        Timer = 0;
                        state = FSM.shoot;
                        idletime = 1;
                    }
                }
                else if (state == FSM.shoot)
                {
                    if (Timer > idletime)
                    {
                        Instantiate(bullet, transform.position, transform.rotation);
                        Timer = 0;
                        state = FSM.idle;
                        idletime = 2;
                    }
                }
            }
            else
            {
                state = FSM.hurt;
                CancelInvoke();
            }
        }
        else
        {
            state = FSM.win;
            CancelInvoke();
        }
        if (state == FSM.win)
        {
            if (chr.PlayerHp > 0)
            {
                state = FSM.idle;
            }
        }
        if (state == FSM.hurt)
        {
            if (chr.power)
            {
                state = FSM.idle;
            }
        }
        if (enemyhp.hp <= 0)
        {
            Destroy(Instantiate(exp, transform.position, Quaternion.identity), 1);
            this.gameObject.SetActive(false);
            //Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerAttack")
        {
            ads.PlayOneShot(hurt);
        }
    }
}
