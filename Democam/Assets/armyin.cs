﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class armyin : MonoBehaviour
{
    public GameObject solider;
    public GameObject sentrygun;
    public GameObject sniper;
    public GameObject leader;
    public GameObject boss;
    public GameObject bigboss;
    public GameObject floatgun;
    public GameObject sentrymom;
    public float timer;
    public int enemynum;
    public GameObject[] enemys;
    public GameObject door;
    public float nowTime;
    //執行重復方法的次數
    public int count;
    public GameObject doors;
    public GameObject doorb;
    public int fisconnt;
    public GameObject doorsentrygun;


    public PlayableDirector StartTimeLine;
    public PlayableDirector Battle_1_BGM;
    public PlayableDirector Battle_1_Boss_BGM;


    //Battle_1_BGM.Stop;
    //Battle_1_Boss_BGM.Start;

    public AudioClip blackhole;
    AudioSource ads;
    // Start is called before the first frame update
    void Start()
    {
        ads = GetComponent<AudioSource>();
        InvokeRepeating("firstdoor", 16, 0.5f);
        Invoke("firstattack", 15);
        timer = 35;
        nowTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        enemys = GameObject.FindGameObjectsWithTag("enemy");
        change();

    }
    void firstdoor()
    {
        Destroy(Instantiate(doorsentrygun, transform.position + new Vector3(-8 + fisconnt * 6, 3, 0), transform.rotation), 0.8f);
        ads.PlayOneShot(blackhole);
        fisconnt += 1;
        if (fisconnt == 4)
        {
            CancelInvoke();
        }

    }
    public void firstattack()
    {
        Destroy(Instantiate(door, transform.position, transform.rotation), 0.5f);
        Destroy(Instantiate(door, transform.position + new Vector3(5, 0, 0), transform.rotation), 0.5f);
        Destroy(Instantiate(door, transform.position + new Vector3(-5, 0, 0), transform.rotation), 0.5f);
        ads.PlayOneShot(blackhole);
        Invoke("birth", 0.5f);
        //Invoke("firstbirth", 1f);

    }
    public void birth()
    {
        Instantiate(solider, transform.position , transform.rotation);
        Instantiate(solider, transform.position + new Vector3(5, 0, 0), transform.rotation);
        Instantiate(solider, transform.position + new Vector3(-5, 0, 0), transform.rotation);
        //Destroy(Instantiate(doors, transform.position + new Vector3(3, 5, 0), transform.rotation), 0.5f);
        //Destroy(Instantiate(doors, transform.position + new Vector3(-3, 5, 0), transform.rotation), 0.5f);
        //Destroy(Instantiate(doors, transform.position + new Vector3(8, 5, 0), transform.rotation), 0.5f);
        //Destroy(Instantiate(doors, transform.position + new Vector3(-8, 5, 0), transform.rotation), 0.5f);
    }
    public void firstbirth()
    {
        Instantiate(sentrygun, transform.position + new Vector3(3, 5, 0), transform.rotation);
        Instantiate(sentrygun, transform.position + new Vector3(-3, 5, 0), transform.rotation);
        Instantiate(sentrygun, transform.position + new Vector3(8, 5, 0), transform.rotation);
        Instantiate(sentrygun, transform.position + new Vector3(-8, 5, 0), transform.rotation);
    }
    public void secondattack()
    {
        Destroy(Instantiate(doors, transform.position + new Vector3(Random.Range(-2,2)+15, Random.Range(-2, 2), Random.Range(-2, 2)), transform.rotation), 0.5f);
        Destroy(Instantiate(doors, transform.position + new Vector3(Random.Range(-2, 2) - 15, Random.Range(-2, 2), Random.Range(-2, 2)), transform.rotation), 0.5f);
        ads.PlayOneShot(blackhole);
        Invoke("secondbirth", 0.5f);
        count += 1;
        if(count==12)
        {
            CancelInvoke();
        }


    }
    public void mom()
    {
        Instantiate(sentrymom, transform.position + new Vector3(0, 0, 5), transform.rotation);
    }
    public void secondbirth()
    {
        Instantiate(floatgun, transform.position + new Vector3(Random.Range(-2, 2)+15, Random.Range(-2, 2), Random.Range(-2, 2)), transform.rotation);
        Instantiate(floatgun, transform.position + new Vector3(Random.Range(-2, 2)-15, Random.Range(-2, 2), Random.Range(-2, 2)), transform.rotation);
    }
    public void thirdattack()
    {
        Destroy(Instantiate(doorb, transform.position, transform.rotation), 0.5f);
        ads.PlayOneShot(blackhole);
        StartTimeLine.Play();
        Invoke("thirdboss", 0.5f);
    }
    public void thirdboss()
    {
        //Instantiate(boss, transform.position, transform.rotation);
        boss.SetActive(true);
    }

    public void change()
    {
        if (enemys.Length == 0)
        {

            if (timer < 0 && enemynum == 0)
            {
                enemynum += 1;
                InvokeRepeating("secondattack", 2,0.5f);
                Destroy(Instantiate(doors, transform.position+new Vector3(0,0,5), transform.rotation), 0.5f);
                Invoke("mom", 0.5f);
                ads.PlayOneShot(blackhole);
                //secondattack();

                timer = 8;
            }
            else
            {
                timer -= Time.deltaTime;
            }
            if (timer < 0 && enemynum == 1)
            {
                Battle_1_BGM.Stop();
                Battle_1_Boss_BGM.Play();
                Invoke("thirdattack", 3);
                //secondattack();
                enemynum += 1;
                timer = 10;
            }
            else
            {
                timer -= Time.deltaTime;
            }
        }
    }
}
