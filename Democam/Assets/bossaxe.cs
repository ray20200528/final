﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossaxe : MonoBehaviour
{
    public enum FSM
    {
        shoot,
        idle,
        hurt,
        win
    }
    public float Timer;
    private float idletime;
    public GameObject bullet;
    public Transform player;
    CharacterController chr;
    private int count;
    public GameObject flash;
    private FSM state;
    public AudioClip fire;
    AudioSource ads;
    // Start is called before the first frame update
    void Start()
    {
        idletime = 10.0f;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        Timer = 0;
        state = FSM.idle;
        ads = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 md = player.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(md);
        transform.rotation = rot;
        Timer += Time.deltaTime;
        if (chr.PlayerHp>0)
        {
            if (chr.power != true)
            {
                if (state == FSM.idle)
                {
                    if (Timer > idletime)
                    {
                        Destroy(Instantiate(flash, transform.position, transform.rotation), 0.8f);
                        
                        InvokeRepeating("shoot", 1, 1);
                        Timer = 0;
                        state = FSM.shoot;
                        idletime = 1;
                    }
                }
                else if (state == FSM.shoot)
                {
                    if (Timer > idletime)
                    {

                        Timer = 0;
                        state = FSM.idle;
                        idletime = 10;
                    }
                }
            }else
            {
                state = FSM.hurt;
                CancelInvoke();
            }
        }else
        {
            state = FSM.win;
            CancelInvoke();
        }
        if(state == FSM.win)
        {
            if(chr.PlayerHp>0)
            {
                state = FSM.idle;
            }
        }
        if(state == FSM.hurt)
        {
            if (chr.power)
            {
                state = FSM.idle;
            }
        }



        //if (Timer > 0)
        //{
        //    //ani.SetBool("fire", false);
        //    Timer -= Time.deltaTime;

        //}
        //else if (Timer <= 0)
        //{
        //    if (chr.PlayerHp > 0)
        //    {
        //        if (chr.power)
        //        {
        //            Destroy(Instantiate(flash, transform.position, transform.rotation), 0.8f);
        //            InvokeRepeating("shoot", 1, 1);
        //            Timer = 10.0f;
        //        }
        //        else
        //        {
        //            Timer += 10 * Time.deltaTime;
        //            CancelInvoke();
        //        }
        //    }
        //    else
        //    {
        //        Timer += 10 * Time.deltaTime;
        //        CancelInvoke();
        //    }
            
        //}
    }
    void shoot()
    {
        
        Instantiate(bullet, transform.position, transform.rotation);
        ads.PlayOneShot(fire);
        count += 1;
        if (count == 5)
        {
            CancelInvoke();
            count = 0;
        }
        
    }
}
