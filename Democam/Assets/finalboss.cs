﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;

public class finalboss : MonoBehaviour
{
    public enum FSM
    {
        None = -1,
        Soon,
        Fullpower,
        Attack,
        Wander,
        Walk,
        Back,
        missile,
        shoot,
        win,
        idle,
        run,
        turn,
        bomb,
        kick,
        hurt,
        dead

    }
    private FSM state;
    private float Idletime;
    private float currenttime;
    public Transform player;
    private int bosshp;
    private float Timer;
    public GameObject bullet;
    public GameObject gun;
    private int r;
    public float distanceToMe;
    private int a;
    Animator ani;
    SkinnedMeshRenderer sm;
    public int lockblood;
    public float rotspeed;
    BossHP enemyhp;
    CharacterController chr;
    public GameObject boom;
    public GameObject missile;
    public GameObject slash;
    public GameObject door;
    public GameObject rightcanon;
    public GameObject leftcanon;
    public GameObject flash;
    public GameObject canon;
    public GameObject bomb;
    public GameObject doosentrygun;
    public GameObject laser;
    public GameObject ring;
    public GameObject energy;
    public GameObject exp;
    public AudioClip hurt;
    public AudioClip enegrys;
    public Material melt;
    public PlayableDirector Dog_2_End;
    public float meltall;
    public AudioClip fires;
    AudioSource ads;
    // Start is called before the first frame update
    void Start()
    {
        gun.SetActive(false);
        ani = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        //Timer = 0.5f;
        //StartCoroutine(FirShotgun());
        //StartCoroutine(FanBarrage(5));
        state = FSM.idle;
        currenttime = 0.0f;
        Idletime = 6;
        sm = GetComponentInChildren<SkinnedMeshRenderer>();
        lockblood = 1;
        enemyhp = GetComponent<BossHP>();
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        r = Random.Range(0, 3);
        ads = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 md = player.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(md);
        distanceToMe = Vector3.Distance(player.transform.position, this.transform.position);
        //melt.SetFloat("_DissolveValue", meltall);
        
        print(state);
        if (chr.PlayerHp > 0)
        {
            if (chr.power != true)
            {

                if (state == FSM.Soon)
                {
                    if (Timer <= 0)
                    {
                        Destroy(Instantiate(door, player.position + new Vector3(0, 0, -5), Quaternion.identity), 0.5f);
                        Invoke("soonback", 0.5f);
                        Timer = 1.5f;
                    }
                    Timer -= Time.deltaTime;
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        Idletime = 3f;
                        state = FSM.Walk;

                        //transform.position = player.position;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.Wander)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    leftcanon.transform.LookAt(player);
                    rightcanon.transform.LookAt(player);
                    distanceToMe = Vector3.Distance(player.transform.position, this.transform.position);
                    if (distanceToMe <= 3)
                    {

                        switch (r)
                        {
                            case 0:
                                transform.Translate(-20 * Time.deltaTime, 0, -20 * Time.deltaTime);
                                ani.SetBool("left", true);
                                break;
                            case 1:
                                transform.Translate(-20 * Time.deltaTime, 0, 0);
                                ani.SetBool("left", true);
                                break;
                            case 2:
                                transform.Translate(20 * Time.deltaTime, 0, -10 * Time.deltaTime);
                                ani.SetBool("right", true);
                                break;
                            case 3:
                                transform.Translate(20 * Time.deltaTime, 0, 0);
                                ani.SetBool("right", true);
                                break;
                        }
                    }
                    else
                    {
                        switch (r)
                        {
                            case 0:
                                ani.SetBool("left", true);
                                transform.Translate(-20 * Time.deltaTime, 0, 0);

                                break;
                            case 1:
                                ani.SetBool("left", true);
                                transform.Translate(-20 * Time.deltaTime, 0, -10 * Time.deltaTime);
                                break;
                            case 2:
                                ani.SetBool("right", true);
                                transform.Translate(20 * Time.deltaTime, 0, -5 * Time.deltaTime);

                                break;
                            case 3:
                                ani.SetBool("right", true);
                                transform.Translate(30 * Time.deltaTime, 0, 0);

                                break;
                        }
                    }
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        ani.SetBool("right", false);
                        ani.SetBool("left", false);
                        CancelInvoke();
                        //ani.SetBool("right", false);
                        //state = FSM.Attack;
                        //InvokeRepeating("Canon", 0.5f, 0.5f);
                        a = Random.Range(0, 3);
                        switch (a)
                        {
                            case 0:
                                state = FSM.shoot;
                                Idletime = 4;
                                Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                                InvokeRepeating("knife", 0.5f, 0.5f);
                                break;
                            case 1:
                                state = FSM.missile;
                                Idletime = 4;
                                Destroy(Instantiate(flash, transform.position, Quaternion.identity), 0.5f);
                                //Destroy(Instantiate(flash, leftcanon.transform.position, Quaternion.identity), 0.5f);
                                InvokeRepeating("fire", 0.5f, 1);
                                break;
                            case 2:
                                Idletime = 2;
                                state = FSM.idle;
                                r = Random.Range(0, 3);
                                break;
                            case 3:
                                currenttime = 0.0f;
                                Idletime = 3f;
                                state = FSM.Walk;
                                break;
                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }


                }
                else if (state == FSM.Attack)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    canon.transform.LookAt(player);
                    leftcanon.transform.forward = transform.forward;
                    rightcanon.transform.forward = transform.forward;
                    if (Timer > 0)
                    {
                        Timer -= Time.deltaTime;
                    }
                    else
                    {
                        //transform.rotation = rot;
                        Vector3 bulletDir = this.transform.position;
                        Quaternion leftRota = Quaternion.AngleAxis(-10, Vector3.up);
                        Quaternion RightRota = Quaternion.AngleAxis(10, Vector3.up);
                        Quaternion upRota = Quaternion.AngleAxis(10, Vector3.forward);
                        Quaternion downRota = Quaternion.AngleAxis(-10, Vector3.forward);
                        for (int i = 0; i < 1; i++)
                        {
                            for (int j = 0; j < 5; j++)
                            {
                                switch (j)
                                {
                                    case 0:
                                        Instantiate(bullet, transform.position, transform.rotation);
                                        break;
                                    case 1:
                                        bulletDir = RightRota * bulletDir;
                                        Instantiate(bullet, transform.position, RightRota * transform.rotation);
                                        break;
                                    case 2:
                                        bulletDir = leftRota * (leftRota * bulletDir);
                                        Instantiate(bullet, transform.position, leftRota * transform.rotation);
                                        bulletDir = RightRota * bulletDir;
                                        break;
                                    case 3:
                                        bulletDir = upRota * bulletDir;
                                        Instantiate(bullet, transform.position, upRota * transform.rotation);
                                        break;
                                    case 4:
                                        bulletDir = downRota * (downRota * bulletDir);
                                        Instantiate(bullet, transform.position, downRota * transform.rotation);
                                        bulletDir = upRota * bulletDir;
                                        break;
                                }
                            }
                        }
                        Timer = 2.0f;
                    }
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        CancelInvoke();
                        Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                        state = FSM.bomb;
                        Idletime = 3;
                        InvokeRepeating("Bomb", 1f, 0.3f);
                        //r = Random.Range(0, 1);
                        //InvokeRepeating("flashs", 0.1f, 0.1f);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }


                }
                else if (state == FSM.Back)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    transform.Translate(-20 * Time.deltaTime, 0, -20 * Time.deltaTime);
                    ani.SetBool("back", true);
                    if (currenttime > Idletime)
                    {
                        CancelInvoke();
                        currenttime = 0.0f;

                        //Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                        //state = FSM.Attack;
                        //InvokeRepeating("Canon", 0.5f, 0.2f);
                        //Idletime = 5;

                        state = FSM.shoot;
                        Idletime = 4;
                        Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                        InvokeRepeating("knife", 0.5f, 0.5f);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.idle)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        switch (r)
                        {
                            case 0:
                                currenttime = 0.0f;
                                Idletime = 3f;
                                state = FSM.Walk;
                                break;
                            case 1:
                                state = FSM.shoot;
                                Idletime = 4;
                                Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                                InvokeRepeating("knife", 0.5f, 0.5f);
                                break;
                            case 2:
                                state = FSM.Wander;
                                Idletime = 4;
                                r = Random.Range(0, 3);
                                if (r < 2)
                                {
                                    Destroy(Instantiate(flash, leftcanon.transform.position, Quaternion.identity), 0.5f);
                                    InvokeRepeating("leftattack", 0.5f, 0.2f);
                                }
                                else
                                {
                                    Destroy(Instantiate(flash, rightcanon.transform.position, Quaternion.identity), 0.5f);
                                    InvokeRepeating("rightattack", 0.5f, 0.2f);
                                }
                                break;
                            case 3:
                                state = FSM.missile;
                                Idletime = 4;
                                Destroy(Instantiate(flash, transform.position, Quaternion.identity), 0.5f);
                                //Destroy(Instantiate(flash, leftcanon.transform.position, Quaternion.identity), 0.5f);
                                InvokeRepeating("fire", 0.5f, 1.5f);
                                r = Random.Range(0, 3);
                                break;
                        }

                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.missile)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    leftcanon.transform.LookAt(player);
                    rightcanon.transform.LookAt(player);
                    transform.Translate(0, 0, -8 * Time.deltaTime);
                    ani.SetBool("back", true);
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        ani.SetBool("back", false);
                        state = FSM.Soon;
                        CancelInvoke();
                        Destroy(Instantiate(door, transform.position, transform.rotation), 0.8f);
                        Invoke("Door", 0.5f);
                        Idletime = 2;
                        Timer = 1.5f;
                        //Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                        //state = FSM.Attack;
                        //InvokeRepeating("Canon", 0.5f, 0.5f);
                        //Idletime = 4;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.shoot)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    if (currenttime > Idletime)
                    {

                        //state = FSM.Wander;
                        //Idletime = 4;
                        //Destroy(Instantiate(flash, rightcanon.transform.position, Quaternion.identity), 0.5f);
                        //Destroy(Instantiate(flash, leftcanon.transform.position, Quaternion.identity), 0.5f);
                        //InvokeRepeating("fire", 0.5f, 1.5f);
                        //r = Random.Range(0, 3);
                        CancelInvoke();
                        currenttime = 0;
                        Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                        state = FSM.Attack;
                        InvokeRepeating("Canon", 0.5f, 0.2f);
                        Idletime = 5;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.run)
                {
                    //transform.rotation = Quaternion.AngleAxis(45, Vector3.up);
                    transform.Translate(Vector3.forward * 60 * Time.deltaTime);
                    if (currenttime > Idletime)
                    {

                        currenttime = 0.0f;
                        state = FSM.Soon;
                        ani.SetBool("run", false);
                        CancelInvoke();
                        Destroy(Instantiate(door, transform.position, transform.rotation), 0.8f);
                        Invoke("Door", 0.5f);
                        Idletime = 2;
                        Timer = 1.5f;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.turn)
                {
                    ani.SetBool("kill", true);

                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    if (currenttime > Idletime)
                    {
                        if (enemyhp.hp > enemyhp.maxhp * 0.5f)
                        {
                            CancelInvoke();
                            currenttime = 0.0f;
                            state = FSM.Fullpower;
                            Idletime = 2;
                            //ani.SetBool("run", true);
                            StartCoroutine(FanBarrage(37));
                            //StartCoroutine(FanBarrage2(37));
                        }
                        else
                        {
                            CancelInvoke();
                            currenttime = 0.0f;
                            state = FSM.Fullpower;
                            Idletime = 5;
                            //ani.SetBool("run", true);
                            StartCoroutine(FanBarrage(37));
                            //StartCoroutine(FanBarrage2(37));
                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.bomb)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    if (currenttime > Idletime)
                    {

                        CancelInvoke();
                        currenttime = 0.0f;
                        state = FSM.Wander;
                        r = Random.Range(0, 3);
                        if (r < 2)
                        {
                            Destroy(Instantiate(flash, leftcanon.transform.position, Quaternion.identity), 0.5f);
                            InvokeRepeating("leftattack", 0.5f, 0.2f);
                        }
                        else
                        {
                            Destroy(Instantiate(flash, rightcanon.transform.position, Quaternion.identity), 0.5f);
                            InvokeRepeating("rightattack", 0.5f, 0.2f);
                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.kick)
                {
                    transform.Rotate(0, 800 * Time.deltaTime, 0);
                    ani.SetBool("right", true);
                    if (currenttime > Idletime)
                    {
                        ani.SetBool("right", false);
                        currenttime = 0.0f;
                        state = FSM.Back;
                        Idletime = 2;
                        InvokeRepeating("Bomb", 1, 0.5f);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.Walk)
                {
                    distanceToMe = Vector3.Distance(player.transform.position, this.transform.position);
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    transform.Translate(0, 0, 0.04f);
                    if (distanceToMe <= 4f)
                    {
                        this.transform.Translate(0, 0, -0.04f);
                        currenttime = 0.0f;
                        Idletime = 0.8f;
                        state = FSM.kick;
                        Destroy(Instantiate(ring, transform.position, transform.rotation), 1f);
                    }
                    if (currenttime > Idletime)
                    {
                        Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                        state = FSM.Attack;
                        InvokeRepeating("Canon", 0.5f, 0.2f);
                        Idletime = 5;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                    if (currenttime > Idletime)
                    {
                        ani.SetBool("right", false);


                        currenttime = 0.0f;
                        state = FSM.Back;
                        Idletime = 2;

                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
               
                if (enemyhp.hp <= 0 && bosshp ==2)
                {
                    CancelInvoke();
                    Dog_2_End.Play();
                    ani.SetBool("dead", true);
                    state = FSM.dead;
                    InvokeRepeating("explo", 0, 0.8f);
                    chr.PlayerHp = chr.mhp;
                    bosshp += 1;
                }
                if (enemyhp.hp <= enemyhp.maxhp * 0.6f && bosshp == 0)
                {
                    CancelInvoke();
                    ads.PlayOneShot(hurt);
                    sm.enabled = true;
                    InvokeRepeating("secondbirth", 0.1f, 1f);
                    Invoke("Energy", 4f);
                    state = FSM.turn;
                    currenttime = 0.0f;
                    Idletime = 5f;
                    lockblood = 0;
                    bosshp += 1;
                    //StartCoroutine(FanBarrage(37));
                    //StartCoroutine(FanBarrage2(37));
                }
                if (enemyhp.hp <= enemyhp.maxhp * 0.3f && bosshp == 1)
                {
                    CancelInvoke();
                    ads.PlayOneShot(hurt);
                    sm.enabled = true;
                    //InvokeRepeating("secondbirth", 0.1f, 1f);
                    Invoke("Energy", 4f);
                    state = FSM.turn;
                    currenttime = 0.0f;
                    Idletime = 5f;
                    lockblood = 0;
                    bosshp += 1;
                    //StartCoroutine(FanBarrage(37));
                    //StartCoroutine(FanBarrage2(37));
                }
            }
            else
            {
                state = FSM.hurt;
                sm.enabled = true;
                CancelInvoke();
                StopAllCoroutines();
                gun.SetActive(false);
                ani.SetBool("kill", false);
                lockblood = 1;
            }
        }
        else
        {
            state = FSM.win;
            sm.enabled = true;
            CancelInvoke();
            StopAllCoroutines();
            gun.SetActive(false);
            ani.SetBool("kill", false);
            lockblood = 1;
        }
        if (state == FSM.win)
        {
            ani.SetBool("win", true);
            if (chr.PlayerHp > 0)
            {
                ani.SetBool("win", false);
                state = FSM.Wander;
            }
        }
        if(state == FSM.hurt)
        {
            if(chr.power)
            {
                state = FSM.idle;
            }
        }
        
        if (state == FSM.Fullpower)
        {
            //transform.LookAt(player);
            transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
            //gun.SetActive(true);
            if (currenttime > Idletime)
            {
                ani.SetBool("kill", false);
                gun.SetActive(false);
                CancelInvoke();
                currenttime = 0.0f;
                state = FSM.Wander;
                r = Random.Range(0, 3);
                lockblood = 1;
                if (r < 2)
                {
                    Destroy(Instantiate(flash, leftcanon.transform.position, Quaternion.identity), 0.5f);
                    InvokeRepeating("leftattack", 0.5f, 0.2f);
                }
                else
                {
                    Destroy(Instantiate(flash, rightcanon.transform.position, Quaternion.identity), 0.5f);
                    InvokeRepeating("rightattack", 0.5f, 0.2f);
                }
                StopAllCoroutines();
            }
            else
            {
                currenttime += Time.deltaTime;
            }


        }
        if(state == FSM.dead)
        {
            
            //meltall += Time.deltaTime/15;
        }


    }
    void Energy()
    {
        Destroy(Instantiate(energy,transform.position, Quaternion.AngleAxis(90, Vector3.right)), 1f);
        ads.PlayOneShot(enegrys);
    }
    void explo()
    {
        Destroy(Instantiate(exp, transform.position + new Vector3(Random.Range(-3, 3), Random.Range(-3, 3), 0), transform.rotation), 0.5f);
    }
    void leftattack()
    {
        Instantiate(bullet, leftcanon.transform.position, leftcanon.transform.rotation);
    }
    void rightattack()
    {
        Instantiate(bullet, rightcanon.transform.position, rightcanon.transform.rotation);
    }
    void flashs()
    {
        Destroy(Instantiate(flash, transform.position , Quaternion.identity), 0.5f);

    }
    void secondbirth()
    {
        Destroy(Instantiate(doosentrygun, transform.position + new Vector3(Random.Range(-10, 10), Random.Range(-10, 10), Random.Range(-10, 10)), transform.rotation), 1);
        Destroy(Instantiate(doosentrygun, transform.position + new Vector3(Random.Range(-10, 10), Random.Range(-10, 10), Random.Range(-10, 10)), transform.rotation), 1);
    }
    void Bomb()
    {
        Instantiate(bomb, transform.position+new Vector3(Random.Range(-10,10), Random.Range(0, 10), 0), Quaternion.identity);
    }
    void fire()
    {
        Instantiate(missile, rightcanon.transform.position, rightcanon.transform.rotation);
        Instantiate(missile, leftcanon.transform.position, leftcanon.transform.rotation);
        //Vector3 bulletDir = canon.transform.position;
        //Quaternion leftRota = Quaternion.AngleAxis(-50, Vector3.up);
        //Quaternion RightRota = Quaternion.AngleAxis(50, Vector3.up);
        //for (int i = 0; i < 1; i++)
        //{
        //    for (int j = 0; j < 3; j++)
        //    {
        //        switch (j)
        //        {
        //            case 0:
        //                Instantiate(missile, canon.transform.position, canon.transform.rotation);
        //                break;
        //            case 1:
        //                bulletDir = RightRota * bulletDir;
        //                Instantiate(missile, canon.transform.position, RightRota * canon.transform.rotation);
        //                break;
        //            case 2:
        //                bulletDir = leftRota * (leftRota * bulletDir);
        //                Instantiate(missile, canon.transform.position, leftRota * canon.transform.rotation);
        //                bulletDir = RightRota * bulletDir;
        //                break;
        //        }
        //    }
        //}
        Instantiate(missile, canon.transform.position, canon.transform.rotation);
    }
    void Dead()
    {
        CancelInvoke();
        Destroy(Instantiate(boom, this.transform.position, this.transform.rotation), 1);
        this.gameObject.SetActive(false);
       
        MainController.GameState += 1;
        Invoke("Return", 2);
    }
    void Return()
    {
        SceneManager.LoadScene(0);
    }
    void Door()
    {
        sm.enabled = false;
    }
    void knife()
    {
        //Instantiate(slash, transform.position, transform.rotation);
        Vector3 bulletDir = this.transform.position;
        Quaternion leftRota = Quaternion.AngleAxis(-30, Vector3.up);
        Quaternion RightRota = Quaternion.AngleAxis(30, Vector3.up);
        for (int i = 0; i < 1; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                switch (j)
                {
                    case 0:
                        Instantiate(slash, transform.position, transform.rotation);
                        break;
                    case 1:
                        bulletDir = RightRota * bulletDir;
                        Instantiate(slash, transform.position, RightRota * transform.rotation);
                        break;
                    case 2:
                        bulletDir = leftRota * (leftRota * bulletDir);
                        Instantiate(slash, transform.position, leftRota * transform.rotation);
                        bulletDir = RightRota * bulletDir;
                        break;
                }
            }
        }
    }
    void shoot()
    {
        Vector3 bulletDir = this.transform.position;
        Quaternion leftRota = Quaternion.AngleAxis(-10, Vector3.up);
        Quaternion RightRota = Quaternion.AngleAxis(10, Vector3.up);
        Quaternion upRota = Quaternion.AngleAxis(10, Vector3.forward);
        Quaternion downRota = Quaternion.AngleAxis(-10, Vector3.forward);
        for (int i = 0; i < 1; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                switch (j)
                {
                    case 0:
                        Instantiate(bullet, transform.position, transform.rotation);
                        break;
                    case 1:
                        bulletDir = RightRota * bulletDir;
                        Instantiate(bullet, transform.position, RightRota * transform.rotation);
                        break;
                    case 2:
                        bulletDir = leftRota * (leftRota * bulletDir);
                        Instantiate(bullet, transform.position, leftRota * transform.rotation);
                        bulletDir = RightRota * bulletDir;
                        break;
                    case 3:
                        bulletDir = upRota * bulletDir;
                        Instantiate(bullet, transform.position, upRota * transform.rotation);
                        break;
                    case 4:
                        bulletDir = downRota * (downRota * bulletDir);
                        Instantiate(bullet, transform.position, downRota * transform.rotation);
                        bulletDir = upRota * bulletDir;
                        break;
                }
            }
        }
    }
    void Canon()
    {
        //Instantiate(bullet, canon.transform.position, canon.transform.rotation);
        Instantiate(bullet, rightcanon.transform.position, rightcanon.transform.rotation);
        Instantiate(bullet, leftcanon.transform.position, leftcanon.transform.rotation);
    }
    void soonback()
    {
        transform.position = player.position+new Vector3(0,0,-5);
        sm.enabled = true;
        
    }

    IEnumerator FirShotgun()
    {
        while (true)
        {
            Vector3 bulletDir = this.transform.position;
            Quaternion leftRota = Quaternion.AngleAxis(-20, Vector3.up);
            Quaternion RightRota = Quaternion.AngleAxis(20, Vector3.up);
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    switch (j)
                    {
                        case 0:
                            Instantiate(missile, transform.position, transform.rotation);
                            break;
                        case 1:
                            bulletDir = RightRota * bulletDir;
                            Instantiate(missile, transform.position, RightRota * transform.rotation);
                            break;
                        case 2:
                            bulletDir = leftRota * (leftRota * bulletDir);
                            Instantiate(missile, transform.position, leftRota * transform.rotation);
                            bulletDir = RightRota * bulletDir;
                            break;

                    }
                }
                yield return new WaitForSeconds(1f);
            }
        }
    }
    IEnumerator FanBarrage(int num)
    {
        while (true)
        {
            Vector3 bulletDir = Vector3.right;
            ads.PlayOneShot(fires);


            float offset = num % 2 != 0 ? 10 : 5;

            float firstOffset = (num % 2) == 0 ? offset : 0;

            int left = (num % 2) == 0 ? -1 : 1;
            for (int i = 0; i < num; i++)
            {

                Quaternion dir = Quaternion.AngleAxis(left * firstOffset, bulletDir);

                Instantiate(laser, transform.position, dir * Quaternion.AngleAxis(90, Vector3.right));
                //Instantiate(laser, transform.position, dir * transform.rotation);

                if (left > 0)
                {
                    firstOffset += offset;
                }
                left = -left;
            }
            yield return new WaitForSeconds(1f);
        }
        
    }
    IEnumerator FanBarrage2(int num)
    {
        while (true)
        {
            Vector3 bulletDir = Vector3.forward;



            float offset = num % 2 != 0 ? 10 : 5;

            float firstOffset = (num % 2) == 0 ? offset : 0;

            int left = (num % 2) == 0 ? -1 : 1;
            for (int i = 0; i < num; i++)
            {

                Quaternion dir = Quaternion.AngleAxis(left * firstOffset, bulletDir);

                Instantiate(laser, transform.position, dir);
                //Instantiate(laser, transform.position, dir * transform.rotation);

                if (left > 0)
                {
                    firstOffset += offset;
                }
                left = -left;
            }
            yield return new WaitForSeconds(1f);
        }

    }
    public void SpiralBarrage()
    {
        StartCoroutine(Spiral());
    }

    IEnumerator Spiral()
    {
        //子弹发射初始方向
        Vector3 bulletDir = Vector3.up;
        //螺旋次数
        int SpiraCount = 36;
        //角度偏移量
        float offset = 360 / SpiraCount;
        //初始弹幕爆炸时间
        //float boomTime = 0.002f;
        ////时间增量
        //float boomTimeIncrement = 0.0005f;
        //螺旋时间间隔
        float time = 0f;

        for (int i = 0; i < SpiraCount; i++)
        {
            //计算旋转后的方向
            Quaternion dir = Quaternion.AngleAxis(i * offset, bulletDir);
            //GameObject go = ObjectPool.Instance.GetObj(ShootConst.BULLETNAMES[bulletID]);
            ////设置子弹位置
            //go.transform.position = firPosition;
            Instantiate(bullet, transform.position, dir * this.transform.rotation);
            //设置子弹旋转
            //fanBarrage.qua = dir;
            ////开火
            //fanBarrage.Fire(num);
            //暂停
            yield return new WaitForSeconds(time);
        }
    }
    private void OnDrawGizmos()
    {
        Debug.DrawLine(this.transform.position, this.transform.position + transform.forward * 30.0f, Color.green);
    }
}
