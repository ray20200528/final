﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sentryson : MonoBehaviour
{
    public enum FSM
    {
        shoot,
        idle,
        hurt,
        win
    }
    public Transform player;
    public float Timer;
    public GameObject bullet;
    private FSM state;
    private float idletime;
    public float rotspeed;
    
    CharacterController chr;
    public GameObject exp;
    public GameObject mom;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        Timer = 0.0f;
        idletime = 2;
        state = FSM.idle;
        mom = GameObject.Find("sentrymom(Clone)");
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 md = player.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(md);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
        Timer += Time.deltaTime;
        if (chr.PlayerHp > 0)
        {
            if (chr.power != true)
            {
                if (state == FSM.idle)
                {
                    if (Timer > idletime)
                    {


                        Timer = 0;
                        state = FSM.shoot;
                        idletime = 1;
                    }
                }
                else if (state == FSM.shoot)
                {
                    if (Timer > idletime)
                    {
                        Instantiate(bullet, transform.position, transform.rotation);
                        Timer = 0;
                        state = FSM.idle;
                        idletime = 2;
                    }
                }
            }
            else
            {
                state = FSM.hurt;
                
            }
        }
        else
        {
            state = FSM.win;
            
        }
        if (state == FSM.win)
        {
            if (chr.PlayerHp > 0)
            {
                state = FSM.idle;
            }
        }
        if (state == FSM.hurt)
        {
            if (chr.power)
            {
                state = FSM.idle;
            }
        }
        //transform.Translate(0.05f, 0, 0);
        if (mom.activeInHierarchy == true)
        {
            //Destroy(Instantiate(exp, transform.position, Quaternion.identity), 1);
            //this.gameObject.SetActive(false);
            //Destroy(gameObject);
        }else
        {
            Destroy(Instantiate(exp, transform.position, Quaternion.identity), 1);
            this.gameObject.SetActive(false);
            Destroy(gameObject);
        }
        if(mom.gameObject == null)
        {
            Destroy(Instantiate(exp, transform.position, Quaternion.identity), 1);
            this.gameObject.SetActive(false);
            Destroy(gameObject);
        }

    }

}
