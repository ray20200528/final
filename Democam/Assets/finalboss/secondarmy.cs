﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class secondarmy : MonoBehaviour
{
    public GameObject solider;
    public GameObject sentrygun;
    public GameObject sniper;
    public GameObject leader;
    public GameObject boss;
    public GameObject bigboss;
    public GameObject floatgun;
    public GameObject sentrymom;
    public float timer;
    public int enemynum;
    public GameObject[] enemys;
    public GameObject door;
    public float nowTime;
    //執行重復方法的次數
    public int count;
    public GameObject doors;
    public GameObject doorb;
    public GameObject finalboss;
    public AudioClip blackhole;
    AudioSource ads;


    public PlayableDirector Dog_1_Battle_Start;
    public PlayableDirector Dog_1_Battle_BGM_Start;
   
    public PlayableDirector Normal_BGM;

    //public PlayableDirector StartTimeLine;
    // Start is called before the first frame update
    void Start()
    {

        ads = GetComponent<AudioSource>();
        Invoke("firstattack", 15);
        timer = 35;
        nowTime = Time.time;
        MainController.GameState = 2;
    }

    // Update is called once per frame
    void Update()
    {
        enemys = GameObject.FindGameObjectsWithTag("enemy");
        change();
    }
    void finalwar()
    {
        finalboss.SetActive(true);
    }
    public void firstattack()
    {
        //Destroy(Instantiate(door, transform.position, transform.rotation), 0.5f);
        Destroy(Instantiate(door, transform.position + new Vector3(5, 0, 0), transform.rotation), 0.5f);
        Destroy(Instantiate(door, transform.position + new Vector3(-5, 0, 0), transform.rotation), 0.5f);
        Destroy(Instantiate(door, transform.position, transform.rotation), 0.5f);
       
        ads.PlayOneShot(blackhole);
        Invoke("birth", 0.5f);
        Invoke("firstbirth", 1f);

    }
    public void birth()
    {
        Instantiate(sentrymom, transform.position, transform.rotation);
        Instantiate(solider, transform.position + new Vector3(5, 0, 0), transform.rotation);
        Instantiate(solider, transform.position + new Vector3(-5, 0, 0), transform.rotation);
        //Destroy(Instantiate(doors, transform.position + new Vector3(3, 5, 0), transform.rotation), 0.5f);
        //Destroy(Instantiate(doors, transform.position + new Vector3(-3, 5, 0), transform.rotation), 0.5f);
        Destroy(Instantiate(doors, transform.position + new Vector3(8, 3, 0), transform.rotation), 0.5f);
        Destroy(Instantiate(doors, transform.position + new Vector3(-8, 3, 0), transform.rotation), 0.5f);
        ads.PlayOneShot(blackhole);
    }
    public void firstbirth()
    {
        //Instantiate(sniper, transform.position + new Vector3(3, 5, 0), transform.rotation);
        //Instantiate(sniper, transform.position + new Vector3(-3, 5, 0), transform.rotation);
        Instantiate(sniper, transform.position + new Vector3(8, 3, 0), transform.rotation);
        Instantiate(sniper, transform.position + new Vector3(-8, 3, 0), transform.rotation);
    }
    public void secondattack()
    {
        Destroy(Instantiate(doors, transform.position + new Vector3(-2+3*count, Random.Range(-2, 2), 0), transform.rotation), 0.5f);
        ads.PlayOneShot(blackhole);
        Invoke("secondbirth", 0.5f);
        count += 1;
        if (count == 4)
        {
            CancelInvoke();
        }
    }
    public void mom()
    {
        Instantiate(leader, transform.position + new Vector3(0, 0, 5), transform.rotation);
        Instantiate(sniper, transform.position + new Vector3(5, 0, 5), transform.rotation);
        Instantiate(solider, transform.position + new Vector3(-5, 0, 5), transform.rotation);
    }
    public void secondbirth()
    {
        Instantiate(sentrygun, transform.position + new Vector3(-2 + 3 * count, Random.Range(-2, 2), 0), transform.rotation);
    }
    public void thirdattack()
    {
        Destroy(Instantiate(doorb, transform.position, transform.rotation), 0.8f);
        ads.PlayOneShot(blackhole);
        //StartTimeLine.Play();
        Invoke("thirdboss", 0.5f);
    }
    public void thirdboss()
    {
        
        bigboss.SetActive(true);
        
    }

    public void change()
    {
        if (enemys.Length == 0)
        {

            if (timer < 0 && enemynum == 0)
            {
                enemynum += 1;
                InvokeRepeating("secondattack", 2, 0.5f);
                Destroy(Instantiate(doors, transform.position + new Vector3(5, 0, 5), transform.rotation), 0.5f);
                Destroy(Instantiate(doors, transform.position + new Vector3(0, 0, 5), transform.rotation), 0.5f);
                Destroy(Instantiate(doors, transform.position + new Vector3(-5, 0, 5), transform.rotation), 0.5f);
                ads.PlayOneShot(blackhole);
                Invoke("mom", 0.5f);

                //secondattack();

                timer = 8;
            }
            else
            {
                timer -= Time.deltaTime;
            }
            if (timer < 0 && enemynum == 1)
            {
                Dog_1_Battle_BGM_Start.Play();
                Normal_BGM.Stop();
                Dog_1_Battle_Start.Play();
                Invoke("thirdattack", 2);
                //secondattack();
                enemynum += 1;
                timer = 8;
            }
            else
            {
                timer -= Time.deltaTime;
            }
            if (timer < 0 && enemynum == 2)
            {
                //Invoke("finalwar", 1);
                enemynum += 1;
            }
        }
    }
}

