﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class birth : MonoBehaviour
{
    public GameObject sentrygun;
    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        timer = 0.5f;
    }

    // Update is called once per frame
    void Update()
    {
        if(timer<=0)
        {
            timer = 2;
            Instantiate(sentrygun, transform.position, Quaternion.identity);

        }
        timer -= Time.deltaTime;
    }
}
