﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FloatingBar : MonoBehaviour
{
    Camera camera;
    Transform target;
    Image image;
    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
        image = GetComponent<Image>();
    }

    public void SetFollowTarget(Transform t)
    {
        target = t;
    }

    // Update is called once per frame
    public Vector3 CalculatePositon()
    {
        Vector3 pos = camera.WorldToScreenPoint(target.position);
        return pos;
    }

    public void LateUpdate()
    {
        Vector3 tPos = target.position;
        tPos.y += 1.0f;
        Vector3 pos = camera.WorldToScreenPoint(tPos);
        if(pos.z < 0)
        {
            image.enabled = false;
        } else
        {
            image.enabled = true;
        }
        transform.position = pos;
    }
}
