﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FloatingText : MonoBehaviour
{
    private Camera cam;
    private TMP_Text tmp_text;
    public float moveSpeed;
    private float currentTime;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 vpos = transform.position;
        vpos.y += moveSpeed*Time.deltaTime;
        transform.position = vpos;
        currentTime += Time.deltaTime;
        if(currentTime > 0.5f)
        {
            Destroy(gameObject);
        }
    }

    public void Spawn(Camera c, Vector3 worldPos, string sText)
    {
        tmp_text = GetComponent<TMP_Text>();
        cam = c;
        Vector3 tPos = worldPos;
        tPos.y += 1.0f;
        tmp_text.text = sText;
        currentTime = 0.0f;
        Vector3 pos = cam.WorldToScreenPoint(tPos);
        if (pos.z < 0)
        {
            tmp_text.enabled = false;
        }
        else
        {
            tmp_text.enabled = true;
        }
        if (UImain.m_Instance.mainCanvas.renderMode == RenderMode.ScreenSpaceCamera)
        {

            Vector3 UIPos;
            RectTransformUtility.ScreenPointToWorldPointInRectangle(UImain.m_Instance.rootUI, pos, UImain.m_Instance.uiCam, out UIPos);
            transform.position = UIPos;

        }
        else if (UImain.m_Instance.mainCanvas.renderMode == RenderMode.ScreenSpaceOverlay)
        {
            transform.position = pos;

        }
    }
}
