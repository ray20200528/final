﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class bigboss : MonoBehaviour
{
    public enum FSM
    {
        None = -1,
        Soon,
        Fullpower,
        Attack,
        Wander,
        Walk,
        Back,
        missile,
        shoot,
        win,
        idle,
        run,
        turn,
        hurt,
        start,
        dead
    }
    private FSM state;
    private float Idletime;
    private float currenttime;
    public Transform player;
    private int bosshp;
    private float Timer;
    public GameObject bullet;
    public GameObject gun;
    private int r;
    public float distanceToMe;
    private int a;
    Animator ani;
    SkinnedMeshRenderer sm;
    public int power;
    public float rotspeed;
    EnemyHP enemyhp;
    CharacterController chr;
    public GameObject boom;
    public GameObject missile;
    public GameObject slash;
    public GameObject door;
    public GameObject rightcanon;
    public GameObject leftcanon;
    public GameObject flash;
    public GameObject canon;
    public GameObject bomb;
    public GameObject doosentrygun;
    public GameObject runball;
    public GameObject exp;
    public AudioClip blackhole;
    public AudioClip run;
    public AudioClip hurt;
    AudioSource ads;
    ami amis;


    public PlayableDirector Dog_1_End;
    public PlayableDirector Dog_1_BGM;
    public PlayableDirector Final_BGM;


    // Start is called before the first frame update
    void Start()
    {
        runball.SetActive(false);
        ani = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        //Timer = 0.5f;
        //StartCoroutine(FirShotgun());
        //StartCoroutine(FanBarrage(5));
        state = FSM.start;
        currenttime = 0.0f;
        Idletime = 3;
        sm = GetComponentInChildren<SkinnedMeshRenderer>();
        enemyhp = GetComponent<EnemyHP>();
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        r = Random.Range(0, 3);
        ads = GetComponent<AudioSource>();
        amis = GameObject.Find("一条綾香").GetComponent<ami>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 md = player.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(md);
        //transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
        //transform.Rotate(0, 1, 0);
        //if (Timer >= 0)
        //{
        //    Timer -= Time.deltaTime;
        //}
        //else if (Timer <= 0)
        //{
        //    //SpiralBarrage();
        //    Timer = 1.0f;
        //}
        Debug.Log("Current State " + state);
        if (chr.PlayerHp > 0)
        {
            if (chr.power != true)
            {
                if (state == FSM.Soon)
                {
                    if (Timer <= 0)
                    {
                        Destroy(Instantiate(door, player.position, Quaternion.identity), 0.5f);
                        ads.PlayOneShot(blackhole);
                        Timer = 1.5f;
                    }
                    Timer -= Time.deltaTime;
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        Idletime = Random.Range(1.0f, 2.0f);
                        state = FSM.Back;
                        sm.enabled = true;
                        transform.position = player.position;
                        InvokeRepeating("shotgun", 0.1f, 0.3f);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.Wander)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    distanceToMe = Vector3.Distance(player.transform.position, this.transform.position);
                    if (distanceToMe <= 3)
                    {

                        switch (r)
                        {
                            case 0:
                                transform.Translate(-20 * Time.deltaTime, 0, -20 * Time.deltaTime);
                                ani.SetBool("left", true);
                                break;
                            case 1:
                                transform.Translate(20 * Time.deltaTime, 0, -20 * Time.deltaTime);
                                ani.SetBool("right", true);
                                break;
                            case 2:
                                transform.Translate(0, 0, -30 * Time.deltaTime);
                                break;
                            case 3:
                                transform.Translate(30 * Time.deltaTime, 0, -20 * Time.deltaTime);
                                ani.SetBool("right", true);
                                break;
                        }
                    }
                    else
                    {
                        switch (r)
                        {
                            case 0:
                                transform.Translate(-20 * Time.deltaTime, 0, 0);
                                ani.SetBool("left", true);
                                break;
                            case 1:
                                transform.Translate(20 * Time.deltaTime, 0, 0);
                                ani.SetBool("right", true);
                                break;
                            case 2:
                                transform.Translate(0, 0, -5 * Time.deltaTime);
                                break;
                            case 3:
                                transform.Translate(30 * Time.deltaTime, 0, 0);
                                ani.SetBool("right", true);
                                break;
                        }
                    }
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        ani.SetBool("right", false);
                        ani.SetBool("left", false);
                        CancelInvoke();
                        //ani.SetBool("right", false);
                        //state = FSM.Attack;
                        //InvokeRepeating("Canon", 0.5f, 0.5f);
                        a = Random.Range(0, 2);
                        switch (a)
                        {
                            case 0:
                                state = FSM.shoot;
                                Idletime = 4;
                                Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                                InvokeRepeating("knife", 0.5f, 0.5f);
                                break;
                            case 1:
                                state = FSM.missile;
                                Idletime = 4;
                                Destroy(Instantiate(flash, rightcanon.transform.position, Quaternion.identity), 0.5f);
                                Destroy(Instantiate(flash, leftcanon.transform.position, Quaternion.identity), 0.5f);
                                InvokeRepeating("fire", 0.5f, 1);
                                break;
                            case 2:
                                Idletime = 1;
                                state = FSM.idle;
                                r = Random.Range(0, 3);
                                break;

                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }


                }
                //else if (state == FSM.Fullpower)
                //{

                //    ani.SetBool("kill", true);
                //    //SpiralBarrage();
                //    //StartCoroutine(FanBarrage(37));
                //    if (Timer >= 0)
                //    {
                //        Timer -= Time.deltaTime;
                //    }
                //    else if (Timer <= 0)
                //    {
                //        SpiralBarrage();
                //        Timer = 0.5f;
                //    }
                //    if (currenttime > Idletime)
                //    {
                //        ani.SetBool("kill", false);
                //        currenttime = 0.0f;
                //        state = FSM.Wander;
                //    }
                //    else
                //    {
                //        currenttime += Time.deltaTime;
                //    }


                //}
                else if (state == FSM.Attack)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    canon.transform.LookAt(player);
                    leftcanon.transform.forward = transform.forward;
                    rightcanon.transform.forward = transform.forward;
                    if (Timer > 0)
                    {
                        Timer -= Time.deltaTime;
                    }
                    else
                    {
                        //transform.rotation = rot;
                        Vector3 bulletDir = this.transform.position;
                        Quaternion leftRota = Quaternion.AngleAxis(-10, Vector3.up);
                        Quaternion RightRota = Quaternion.AngleAxis(10, Vector3.up);
                        Quaternion upRota = Quaternion.AngleAxis(10, Vector3.forward);
                        Quaternion downRota = Quaternion.AngleAxis(-10, Vector3.forward);
                        for (int i = 0; i < 1; i++)
                        {
                            for (int j = 0; j < 5; j++)
                            {
                                switch (j)
                                {
                                    case 0:
                                        Instantiate(bullet, transform.position, transform.rotation);
                                        break;
                                    case 1:
                                        bulletDir = RightRota * bulletDir;
                                        Instantiate(bullet, transform.position, RightRota * transform.rotation);
                                        break;
                                    case 2:
                                        bulletDir = leftRota * (leftRota * bulletDir);
                                        Instantiate(bullet, transform.position, leftRota * transform.rotation);
                                        bulletDir = RightRota * bulletDir;
                                        break;
                                    case 3:
                                        bulletDir = upRota * bulletDir;
                                        Instantiate(bullet, transform.position, upRota * transform.rotation);
                                        break;
                                    case 4:
                                        bulletDir = downRota * (downRota * bulletDir);
                                        Instantiate(bullet, transform.position, downRota * transform.rotation);
                                        bulletDir = upRota * bulletDir;
                                        break;
                                }
                            }
                        }
                        Timer = 2.0f;
                    }
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        CancelInvoke();
                        state = FSM.turn;
                        Idletime = 0.5f;
                        r = Random.Range(0, 1);
                        InvokeRepeating("flashs", 0.1f, 0.1f);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;

                    }


                }
                else if (state == FSM.Back)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    transform.Translate(-20 * Time.deltaTime, 0, -15 * Time.deltaTime);
                    ani.SetBool("left", true);
                    if (currenttime > Idletime)
                    {
                        if (enemyhp.hp <= enemyhp.maxhp * 0.5f && bosshp == 2)
                        {
                            state = FSM.turn;
                            r = 1;
                            Idletime = 0.5f;
                            bosshp += 1;
                        }
                        else
                        {
                            CancelInvoke();
                            currenttime = 0.0f;
                            ani.SetBool("left", false);
                            //state = FSM.shoot;
                            //Idletime = 4;
                            //Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                            //InvokeRepeating("knife", 0.5f, 0.5f);
                            state = FSM.Wander;
                            r = Random.Range(0, 3);
                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.idle)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    if (currenttime > Idletime)
                    {

                        switch (r)
                        {
                            case 0:
                                state = FSM.shoot;
                                Idletime = 4;
                                Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                                InvokeRepeating("knife", 0.5f, 0.5f);
                                break;
                            case 1:
                                state = FSM.Wander;
                                r = Random.Range(0, 3);
                                Idletime = 3;
                                break;
                            case 2:
                                state = FSM.missile;
                                Idletime = 4;
                                Destroy(Instantiate(flash, rightcanon.transform.position, Quaternion.identity), 0.5f);
                                Destroy(Instantiate(flash, leftcanon.transform.position, Quaternion.identity), 0.5f);
                                InvokeRepeating("fire", 0.5f, 1.5f);
                                r = Random.Range(0, 3);
                                break;
                        }

                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.missile)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    leftcanon.transform.forward = transform.forward;
                    rightcanon.transform.forward = transform.forward;
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        state = FSM.idle;
                        Idletime = 2;
                        r = Random.Range(0, 3);
                        CancelInvoke();
                        //Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                        //state = FSM.Attack;
                        //InvokeRepeating("Canon", 0.5f, 0.5f);
                        //Idletime = 4;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.shoot)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    if (currenttime > Idletime)
                    {
                        CancelInvoke();
                        currenttime = 0.0f;
                        Destroy(Instantiate(flash, transform.position, transform.rotation), 0.5f);
                        state = FSM.Attack;
                        InvokeRepeating("Canon", 0.5f, 0.5f);
                        Idletime = 4;
                        //state = FSM.Wander;
                        //Idletime = 4;
                        //Destroy(Instantiate(flash, rightcanon.transform.position, Quaternion.identity), 0.5f);
                        //Destroy(Instantiate(flash, leftcanon.transform.position, Quaternion.identity), 0.5f);
                        //InvokeRepeating("fire", 0.5f, 1.5f);
                        //r = Random.Range(0, 4);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.run)
                {
                    //transform.rotation = Quaternion.AngleAxis(45, Vector3.up);
                    transform.Translate(Vector3.forward * 60 * Time.deltaTime);
                    
                    if (currenttime > Idletime)
                    {
                        runball.SetActive(false);
                        currenttime = 0.0f;
                        state = FSM.Soon;
                        ani.SetBool("run", false);
                        CancelInvoke();
                        Destroy(Instantiate(door, transform.position, transform.rotation), 0.8f);
                        //ads.PlayOneShot(blackhole);
                        Invoke("Door", 0.5f);
                        Idletime = 2;
                        Timer = 1.5f;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if(state == FSM.start)
                {
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        state = FSM.idle;
                        Idletime = 3;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.turn)
                {

                    switch (r)
                    {
                        case 0:
                            transform.Rotate(Vector3.up, -90 * Time.deltaTime);
                            break;
                        case 1:
                            transform.Rotate(Vector3.up, 90 * Time.deltaTime);
                            break;
                    }

                    if (currenttime > Idletime)
                    {
                        if (enemyhp.hp <= enemyhp.maxhp * 0.5f && bosshp == 1)
                        {
                            runball.SetActive(true);
                            bosshp += 1;
                            currenttime = 0.0f;
                            state = FSM.run;
                            InvokeRepeating("Bomb", 0.1f, 0.2f);
                            Idletime = 1;
                            ani.SetBool("run", true);
                            ads.PlayOneShot(run);
                        }
                        else
                        {
                            runball.SetActive(true);
                            CancelInvoke();
                            currenttime = 0.0f;
                            state = FSM.run;
                            InvokeRepeating("Bomb", 0.1f, 0.2f);
                            Idletime = 1;
                            ani.SetBool("run", true);
                            ads.PlayOneShot(run);
                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                if (enemyhp.hp <= 0&& power ==0)
                {
                    CancelInvoke();
                    state = FSM.dead;
                    amis.Awakening = true;
                    chr.PlayerHp = chr.mhp;
                    ani.SetBool("dead", true);
                    InvokeRepeating("explo", 0, 0.8f);
                    sm.enabled = true;
                    runball.SetActive(false);
                    power += 1;
                    Dog_1_End.Play();
                    Dog_1_BGM.Stop();
                    Final_BGM.Play();
                }
                if (enemyhp.hp <= enemyhp.maxhp * 0.5f && bosshp == 0)
                {
                    CancelInvoke();
                    ani.SetBool("kill", true);
                    InvokeRepeating("secondbirth", 0, 1);
                    state = FSM.turn;
                    r = 0;
                    Idletime = 0.5f;

                    //lockblood = 0;
                    bosshp += 1;

                }

            }else
            {
                state = FSM.hurt;
                sm.enabled = true;
                CancelInvoke();
                runball.SetActive(false);
             
            }
        }
        else
        {
            state = FSM.win;
            CancelInvoke();
            sm.enabled = true;
            runball.SetActive(false);
        }
        if (state == FSM.win)
        {
            ani.SetBool("win", true);
            if (chr.PlayerHp > 0)
            {
                ani.SetBool("win", false);
                state = FSM.Wander;
            }
        }
        if (state == FSM.hurt)
        {
            if (chr.power)
            {
                state = FSM.idle;
            }
        }
        if (state == FSM.Fullpower)
        {

            if (currenttime > Idletime)
            {
                ani.SetBool("kill", false);
                CancelInvoke();
                currenttime = 0.0f;

                state = FSM.Wander;

            }
            else
            {
                currenttime += Time.deltaTime;
            }


        }


    }
    void flashs()
    {
        Destroy(Instantiate(flash, transform.position + new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), 0), Quaternion.identity), 0.5f);

    }
    void explo()
    {
        Destroy(Instantiate(exp, transform.position + new Vector3(Random.Range(-3, 3), Random.Range(-3, 3), 0),transform.rotation), 0.8f);
    }
    void secondbirth()
    {
        Destroy(Instantiate(doosentrygun, transform.position + new Vector3(Random.Range(-10, 10), Random.Range(-10, 10), Random.Range(-10, 10)), transform.rotation), 1);
        Destroy(Instantiate(doosentrygun, transform.position + new Vector3(Random.Range(-10, 10), Random.Range(-10, 10), Random.Range(-10, 10)), transform.rotation), 1);
    }
    void Bomb()
    {
        Instantiate(bomb, transform.position, Quaternion.identity);
    }
    void fire()
    {
        Instantiate(missile, rightcanon.transform.position, rightcanon.transform.rotation);
        Instantiate(missile, leftcanon.transform.position, leftcanon.transform.rotation);
    }
    void Dead()
    {
        CancelInvoke();
        Destroy(Instantiate(boom, this.transform.position, this.transform.rotation), 1);
        this.gameObject.SetActive(false);
    }
    void Door()
    {
        sm.enabled = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerAttack")
        {
            ads.PlayOneShot(hurt);
        }
    }
    void knife()
    {
        //Instantiate(slash, transform.position, transform.rotation);
        Vector3 bulletDir = this.transform.position;
        Quaternion leftRota = Quaternion.AngleAxis(-30, Vector3.up);
        Quaternion RightRota = Quaternion.AngleAxis(30, Vector3.up);
        for (int i = 0; i < 1; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                switch (j)
                {
                    case 0:
                        Instantiate(slash, transform.position, transform.rotation);
                        break;
                    case 1:
                        bulletDir = RightRota * bulletDir;
                        Instantiate(slash, transform.position, RightRota * transform.rotation);
                        break;
                    case 2:
                        bulletDir = leftRota * (leftRota * bulletDir);
                        Instantiate(slash, transform.position, leftRota * transform.rotation);
                        bulletDir = RightRota * bulletDir;
                        break;
                }
            }
        }
    }
    void shoot()
    {
        Vector3 bulletDir = this.transform.position;
        Quaternion leftRota = Quaternion.AngleAxis(-10, Vector3.up);
        Quaternion RightRota = Quaternion.AngleAxis(10, Vector3.up);
        Quaternion upRota = Quaternion.AngleAxis(10, Vector3.forward);
        Quaternion downRota = Quaternion.AngleAxis(-10, Vector3.forward);
        for (int i = 0; i < 1; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                switch (j)
                {
                    case 0:
                        Instantiate(bullet, transform.position, transform.rotation);
                        break;
                    case 1:
                        bulletDir = RightRota * bulletDir;
                        Instantiate(bullet, transform.position, RightRota * transform.rotation);
                        break;
                    case 2:
                        bulletDir = leftRota * (leftRota * bulletDir);
                        Instantiate(bullet, transform.position, leftRota * transform.rotation);
                        bulletDir = RightRota * bulletDir;
                        break;
                    case 3:
                        bulletDir = upRota * bulletDir;
                        Instantiate(bullet, transform.position, upRota * transform.rotation);
                        break;
                    case 4:
                        bulletDir = downRota * (downRota * bulletDir);
                        Instantiate(bullet, transform.position, downRota * transform.rotation);
                        bulletDir = upRota * bulletDir;
                        break;
                }
            }
        }
    }
    void Canon()
    {
        Instantiate(bullet, canon.transform.position, canon.transform.rotation);
        Instantiate(bullet, rightcanon.transform.position, rightcanon.transform.rotation);
        Instantiate(bullet, leftcanon.transform.position, leftcanon.transform.rotation);
    }
    void shotgun()
    {
        Vector3 bulletDir = this.transform.position;
        Quaternion leftRota = Quaternion.AngleAxis(-20, Vector3.up);
        Quaternion RightRota = Quaternion.AngleAxis(20, Vector3.up);
        for (int i = 0; i < 1; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                switch (j)
                {
                    case 0:
                        Instantiate(bullet, transform.position, transform.rotation);
                        break;
                    case 1:
                        bulletDir = RightRota * bulletDir;
                        Instantiate(bullet, transform.position, RightRota * transform.rotation);
                        break;
                    case 2:
                        bulletDir = leftRota * (leftRota * bulletDir);
                        Instantiate(bullet, transform.position, leftRota * transform.rotation);
                        bulletDir = RightRota * bulletDir;
                        break;

                }
            }
           
        }
    }

    IEnumerator FirShotgun()
    {
        while (true)
        {
            Vector3 bulletDir = this.transform.position;
            Quaternion leftRota = Quaternion.AngleAxis(-20, Vector3.up);
            Quaternion RightRota = Quaternion.AngleAxis(20, Vector3.up);
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    switch (j)
                    {
                        case 0:
                            Instantiate(bullet, transform.position, transform.rotation);
                            break;
                        case 1:
                            bulletDir = RightRota * bulletDir;
                            Instantiate(bullet, transform.position, RightRota * transform.rotation);
                            break;
                        case 2:
                            bulletDir = leftRota * (leftRota * bulletDir);
                            Instantiate(bullet, transform.position, leftRota * transform.rotation);
                            bulletDir = RightRota * bulletDir;
                            break;

                    }
                }
                yield return new WaitForSeconds(1f);
            }
        }
    }

    IEnumerator FanBarrage(int num)
    {
        while (true)
        {
            Vector3 bulletDir = Vector3.up;



            float offset = num % 2 != 0 ? 10 : 5;

            float firstOffset = (num % 2) == 0 ? offset : 0;

            int left = (num % 2) == 0 ? -1 : 1;
            for (int i = 0; i < num; i++)
            {

                Quaternion dir = Quaternion.AngleAxis(left * firstOffset, bulletDir);

                Instantiate(bullet, transform.position, dir * transform.rotation);

                if (left > 0)
                {
                    firstOffset += offset;
                }
                left = -left;
            }
            yield return new WaitForSeconds(0.5f);
        }
    }
    public void SpiralBarrage()
    {
        StartCoroutine(Spiral());
    }

    IEnumerator Spiral()
    {
        //子弹发射初始方向
        Vector3 bulletDir = Vector3.up;
        //螺旋次数
        int SpiraCount = 36;
        //角度偏移量
        float offset = 360 / SpiraCount;
        //初始弹幕爆炸时间
        //float boomTime = 0.002f;
        ////时间增量
        //float boomTimeIncrement = 0.0005f;
        //螺旋时间间隔
        float time = 0f;

        for (int i = 0; i < SpiraCount; i++)
        {
            //计算旋转后的方向
            Quaternion dir = Quaternion.AngleAxis(i * offset, bulletDir);
            //GameObject go = ObjectPool.Instance.GetObj(ShootConst.BULLETNAMES[bulletID]);
            ////设置子弹位置
            //go.transform.position = firPosition;
            Instantiate(bullet, transform.position, dir * this.transform.rotation);
            //设置子弹旋转
            //fanBarrage.qua = dir;
            ////开火
            //fanBarrage.Fire(num);
            //暂停
            yield return new WaitForSeconds(time);
        }
    }
    private void OnDrawGizmos()
    {
        Debug.DrawLine(this.transform.position, this.transform.position + transform.forward * 30.0f, Color.green);
    }
}
