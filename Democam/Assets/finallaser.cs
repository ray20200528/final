﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class finallaser : MonoBehaviour
{
    enum turn
    {
        turn,
        run,
        win
    }
    private turn state;
    public float speed = 10f;
    public float rotspeed = 80;
    //public GameObject exp;
    public Transform target;
    public float acc = 30;
    public float time = 0.5f;
    CharacterController chr;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").transform;
        //transform.Rotate(-90, 0, 30);
        //transform.Rotate(-90, 0, 0);
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {

        if (chr.PlayerHp > 0)
        {
            if (state == turn.turn)
            {
                //Vector3 tarv = (target.position - this.transform.position).normalized;
                //float a = Vector3.Angle(transform.forward, tarv) / rotspeed;
                //if (a > 0.1f || a < -0.1f)
                //{
                //    transform.forward = Vector3.Slerp(transform.forward, tarv, Time.deltaTime / a).normalized;
                //}
                //else
                //{
                //    speed += acc * Time.deltaTime;
                //    transform.forward = Vector3.Slerp(transform.forward, tarv, 1).normalized;
                //}
                //transform.position += transform.forward * speed * Time.deltaTime;
                transform.position += transform.forward * speed * Time.deltaTime;
                if (time < 0)
                {
                    state = turn.run;
                    Vector3 md = target.transform.position - transform.position;
                    Quaternion rot = Quaternion.LookRotation(md);
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    time = 3;
                }
                else
                {
                    time -= Time.deltaTime;
                }
            }
            else if (state == turn.run)
            {

                transform.Translate(0, 0, 100*Time.deltaTime);
                if (time < 0)
                {
                    Destroy(gameObject);
                }
                else
                {
                    time -= Time.deltaTime;
                }
            }
        }
        else
        {
            state = turn.win;
        }
        if (state == turn.win)
        {
            Destroy(gameObject);
        }
    }
}
