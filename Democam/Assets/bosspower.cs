﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class bosspower : MonoBehaviour
{
    public float Damage;
    public float Resist;
    public amistate cha;
    Grain g;
    private float v;


    //public Image blood;
    public GameObject player;
    private void Start()
    {
        cha = GameObject.Find("一条綾香").GetComponent<amistate>();


    }
    private void Update()
    {
        Resist = cha.PlayerDamageResist;

    }



    void OnTriggerStay(Collider DamageEvent)
    {
        if (DamageEvent.tag == "Player")
        {
            CharacterController chr;
            chr = DamageEvent.gameObject.GetComponent<CharacterController>();
            chr.PlayerHp -= Damage * cha.PlayerDamageResist;
            UImain.m_Instance.SpawnFloatingText(DamageEvent.transform.position, Damage.ToString());
            if (Damage * cha.PlayerDamageResist >= 100)
            {
                chr.intensity = 1.0f;
                //this.gameObject.SetActive(false);
                ScreenShake.isshakeCamera = true;
                //擊中效果
            }
        }
    }
}
