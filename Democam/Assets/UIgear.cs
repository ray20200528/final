﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIgear : MonoBehaviour
{
    public Object floatingBar;
    public Object floatingText;
    public Dropdown dp;
    private List<FloatingBar> fbs = new List<FloatingBar>();
    public static UIgear m_Instance;
    public RectTransform rootUI;
    public Canvas mainCanvas;
    public Camera uiCam;
    private DragEvent currentDragger;
    private void Awake()
    {
        m_Instance = this;
        test(Vector3.one);
    }

    public void setcurrentDrag(DragEvent d)
    {
        currentDragger = d;
    }
    public DragEvent currentDrag()
    {
        return currentDragger;
    }

    void test(Vector2 vv)
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        rootUI = GetComponent<RectTransform>();
        mainCanvas = GetComponent<Canvas>();
    }

    // Update is called once per frame
    
    public void OnButtonClick(Image b)
    {
        Debug.Log(b.name);
    }

    public void OnSliderValueChange(Slider s)
    {

        AudioListener.volume = s.value;
        Debug.Log(s.value);
    }

    public void OnEndEdit(InputField i)
    {

        Dropdown.OptionData data = new Dropdown.OptionData();
        data.text = i.text;
        dp.options.Add(data);

    }

    public void OnDrop(Image m)
    {
        currentDragger.Drop(m.transform);
        currentDragger = null;
        Debug.Log("OnDrop");
    }
}
