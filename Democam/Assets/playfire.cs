﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Effects;

public class playfire : MonoBehaviour
{
    public GameObject bullet;
    public Camera fcam;
    public Vector3 targetPoint;
    public float gatelin;
    public bool fire=true;
    public AudioClip gun;
    AudioSource ads;
    private void Start()
    {
        StartCoroutine(SpawnCoroutine());
        ads = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    //void Start()
    //{
         //StartCoroutine(SpawnCoroutine());
    //}

    // Update is called once per frame
    void Update()
    {
      
    }
    IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            if (Input.GetMouseButton(0))
            {
                if (fire == true)
                {
                    Instantiate(bullet, this.transform.position, transform.rotation);
                    ads.PlayOneShot(gun);
                }
            }

            yield return new WaitForSeconds(gatelin);
        }
    }
}
