﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class boss : MonoBehaviour
{
    public enum FSM
    {
        None = -1,
        Combat,
        Fullpower,
        Attack,
        Wander,
        Walk,
        Back,
        idle,
        win,
        laser,
        turn,
        dead,
        hurt
    }
    private FSM state;
    private float Idletime;
    private float currenttime;
    public Transform player;
    public float Timer;
    public GameObject bullet;
    public GameObject laser;
    public int r;
    public GameObject bossgun;
    public GameObject bossgun2;
    public float distanceToMe;
    public int a;
    public GameObject boom;
    BossHP enemyhp;
    CharacterController chr;
    private Animator ani;
    public float rotspeed,meltall;
    public GameObject knife;
    public GameObject canon;
    public GameObject beam;
    private int broke;
    public GameObject flash;
    private int count;
    public int lockblood;
    public GameObject exp;
    public GameObject energy;
    public Material melt;
    public PlayableDirector DeadTimeLine;
    public AudioClip burn;
    public AudioClip hurt;
    AudioSource ads;
    public AudioClip blackhole;
    // Start is called before the first frame update
    void Start()
    {
        enemyhp = GetComponent<BossHP>();
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        Timer = 2.0f;
        state = FSM.turn;
        currenttime = 0.0f;
        Idletime = 2;
        ani = GetComponent<Animator>();
        laser.SetActive(false);
        lockblood = 1;
        Invoke("overkill", 0.5f);
        ads = GetComponent<AudioSource>();
        meltall = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
        Vector3 md = player.position - transform.position;
        //md.y = 0;
        Quaternion rot = Quaternion.LookRotation(md);
        //melt.SetFloat("_DissolveValue", meltall);
        if (chr.PlayerHp > 0)
        {
            if (chr.power != true)
            {
                if (state == FSM.Combat)
                {
                    transform.Rotate(0, 500 * Time.deltaTime, 0);

                    // Check Dead
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        Idletime = Random.Range(1.0f, 2.0f);
                        state = FSM.Back;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.Wander)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    Vector3 cmd = player.position - canon.transform.position;
                    Quaternion rat = Quaternion.LookRotation(cmd);
                    canon.transform.rotation = Quaternion.Lerp(canon.transform.rotation, rat, rotspeed);
                    //canon.transform.LookAt(player);
                    switch (a)
                    {
                        case 0:
                            transform.Translate(10 * Time.deltaTime, 0, 0);
                            break;
                        case 1:
                            transform.Translate(-10 * Time.deltaTime, 0, 0);
                            break;
                        case 2:
                            transform.Translate(5 * Time.deltaTime, 0, -2 * Time.deltaTime);
                            break;
                        case 3:
                            transform.Translate(5 * Time.deltaTime, 0, -1 * Time.deltaTime);
                            break;
                    }
                    r = Random.Range(0, 1);
                    if (currenttime > Idletime)
                    {
                        CancelInvoke();
                        currenttime = 0.0f;
                        state = FSM.Walk;
                        Idletime = 3;
                        ani.SetBool("run", true);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }


                }
                else if (state == FSM.Attack)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    r = Random.Range(0, 2);
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        CancelInvoke();
                        a = Random.Range(0, 3);
                        state = FSM.Wander;
                        InvokeRepeating("shoot", 0.1f, 0.8f);
                        Idletime = 3;

                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }


                }
                else if (state == FSM.Walk)
                {
                    distanceToMe = Vector3.Distance(player.transform.position, this.transform.position);
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    transform.Translate(0, 0, 0.04f);
                    if (distanceToMe <= 4f)
                    {
                        ani.SetBool("run", false);
                        this.transform.Translate(0, 0, -0.04f);
                        currenttime = 0.0f;
                        Idletime = 0.8f;
                        state = FSM.Combat;
                        Destroy(Instantiate(knife, transform.position, transform.rotation), 0.8f);
                    }
                    if (currenttime > Idletime)
                    {
                        ani.SetBool("run", false);
                        currenttime = 0.0f;
                        state = FSM.Attack;
                        Invoke("flashs", 0.5f);
                        InvokeRepeating("Attack", 1, 0.15f);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.Back)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    transform.Translate(20*Time.deltaTime, 0, -10*Time.deltaTime);
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        r = Random.Range(0, 1);
                        switch (r)
                        {
                            case 0:
                                state = FSM.Attack;
                                Invoke("flashs", 0.5f);
                                InvokeRepeating("Attack", 1, 0.15f);
                                Idletime = 3;
                                break;
                            case 1:
                                state = FSM.idle;
                                break;


                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.idle)
                {
                    a = Random.Range(0, 2);
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;

                        
                        switch (a)
                        {
                            case 0:
                                a = Random.Range(0, 3);
                                state = FSM.Wander;
                                InvokeRepeating("shoot", 0.1f, 0.8f);
                                Idletime = 3;
                                break;
                            case 1:
                                state = FSM.Attack;
                                Invoke("flashs", 0.5f);
                                InvokeRepeating("Attack", 1, 0.15f);
                                Idletime = 3;
                                break;
                            case 2:
                                state = FSM.Walk;
                                ani.SetBool("run", true);
                                break;
                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.laser)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
                    //Quaternion rat = Quaternion.LookRotation(player.position-canon.transform.position);
                    //canon.transform.rotation = Quaternion.Lerp(canon.transform.rotation,rat, 0.05f);
                    canon.transform.LookAt(player);
                    lasers();

                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        laser.SetActive(false);
                        state = FSM.Walk;
                        ani.SetBool("run", true);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.turn)
                {
                    transform.Rotate(0, 350 * Time.deltaTime, 0);
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        Idletime = 3;
                        state = FSM.idle;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                if (enemyhp.hp <= 0 && broke == 3)
                {
                    ani.SetBool("dead", true);
                    state = FSM.dead;
                    CancelInvoke();
                    InvokeRepeating("explotion", 0, 0.8f);
                    chr.PlayerHp = chr.mhp;
                    broke += 1;
                }
                if (enemyhp.hp <= enemyhp.maxhp * 0.5f && broke == 0)
                {
                    Destroy(Instantiate(boom, bossgun.transform.position, Quaternion.identity), 0.8f);
                    Destroy(bossgun);
                    broke += 1;
                }
                if (enemyhp.hp <= enemyhp.maxhp * 0.4f && broke == 1)
                {
                    CancelInvoke();
                    lockblood = 0;
                    state = FSM.Fullpower;
                    currenttime = 0.0f;
                    Idletime = 4;
                    ads.PlayOneShot(hurt);
                    ani.SetBool("kill", true);
                    Invoke("overkill", 0.8f);
                    Invoke("pose", 2f);
                    broke += 1;
                    //HittedMatEffect2 sc = gameObject.GetComponent<HittedMatEffect2>();
                    //if (null == sc)
                    //    sc = gameObject.AddComponent<HittedMatEffect2>();
                    //sc.Active();
                    Invoke("Energy", 3f);

                }
                if (enemyhp.hp <= enemyhp.maxhp * 0.3f && broke == 2)
                {
                    Destroy(Instantiate(boom, bossgun2.transform.position, Quaternion.identity), 0.8f);
                    Destroy(bossgun2);
                    broke += 1;
                }
            }else
            {
                state = FSM.hurt;
                CancelInvoke();
                ani.SetBool("kill", false);
                lockblood = 1;
            }
        }
        else
        {
            state = FSM.win;
            CancelInvoke();
            laser.SetActive(false);
            ani.SetBool("kill", false);
        }
        if (state == FSM.win)
        {
            ani.SetBool("win", true);
            if (chr.PlayerHp > 0)
            {
                ani.SetBool("win", false);
                state = FSM.idle;
                lockblood = 1;
            }
        }
        if (state == FSM.hurt)
        {
            if (chr.power)
            {
                state = FSM.idle;
            }
        }
        if (state == FSM.Fullpower)
        {
            //lockblood = 0;

            //sc.SetColor(Color.yellowwhit);



            transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
            transform.Rotate(0, 2, 0);
            //if (Timer > 0)
            //{
            //    Timer -= Time.deltaTime;
            //}
            //else if (Timer <= 0)
            //{

            //    ani.SetBool("kill", false);

            //    Timer = 10.0f;
            //}
            if (currenttime > Idletime)
            {
                currenttime = 0.0f;
                state = FSM.laser;
                lockblood = 1;
                Idletime = 2.6f;
                laser.SetActive(true);
                ads.PlayOneShot(burn);
            }
            else
            {
                currenttime += Time.deltaTime;
            }


        }
        if(state == FSM.dead)
        {
            
            //meltall += Time.deltaTime / 10;
        }
    }
    
    void Energy()
    {
        Destroy(Instantiate(energy, canon.transform.position,Quaternion.AngleAxis(90, Vector3.right)), 1f);
        ads.PlayOneShot(blackhole);
    }
    void pose()
    {
        ani.SetBool("kill", false);
    }
    void overkill()
    {
        
        Destroy(Instantiate(knife, transform.position, transform.rotation), 0.8f);
        
    }
    void lasers()
    {
        Destroy(Instantiate(beam, canon.transform.position, canon.transform.rotation), 1f);
    }

    private void OnDrawGizmos()
    {
        Debug.DrawLine(this.transform.position, this.transform.position + transform.forward * 30.0f, Color.green);
    }
    public void Dead()
    {
        Destroy(Instantiate(boom, this.transform.position, this.transform.rotation), 1);
        this.gameObject.SetActive(false);
        CancelInvoke();
        SceneManager.LoadScene(0);
        MainController.GameState++;
    }
    public void shoot()
    {
        Instantiate(bullet, canon.transform.position, canon.transform.rotation);
    }
    void explotion()
    {
        Destroy(Instantiate(exp, transform.position + new Vector3(Random.Range(-3, 3), Random.Range(-3, 3), 0), transform.rotation), 0.5f);
    }
    void flashs()
    {
        Destroy(Instantiate(flash, transform.position, transform.rotation), 0.8f);
    }
    void Attack()
    {
        //int count = 0;
        //count += 1;
        //if (count == 5)
        //{
        //    CancelInvoke();
        //}

        Vector3 bulletDir = this.transform.position;
        Quaternion leftRota = Quaternion.AngleAxis(-20, Vector3.up);
        Quaternion RightRota = Quaternion.AngleAxis(20, Vector3.up);
        for (int i = 0; i < 1; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                switch (j)
                {
                    case 0:
                        Instantiate(bullet, transform.position, transform.rotation);
                        break;
                    case 1:
                        bulletDir = RightRota * bulletDir;
                        Instantiate(bullet, transform.position, RightRota * transform.rotation);
                        break;
                    case 2:
                        bulletDir = leftRota * (leftRota * bulletDir);
                        Instantiate(bullet, transform.position, leftRota * transform.rotation);
                        bulletDir = RightRota * bulletDir;
                        break;
                }
            }

        }
        //int count = 0;
        count += 1;
        if (count == 1)
        {
            CancelInvoke();
        }



    }
    void endtimeline()
    {
        DeadTimeLine.Play();
    }
}
