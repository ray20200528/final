﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playersentrygun : MonoBehaviour
{
    public float life;
    public float Timer;
    public GameObject bullet;
    public float rotspeed;
    public GameObject exp;
    public GameObject[] enemys;
    public GameObject boss;
    public GameObject bigboss;
    public GameObject dogboss;
    private int a,b;
    AudioSource ads;
    public AudioClip fire;
    boss sir;
    bigboss bigsir;
    finalboss fsir;
    // Start is called before the first frame update
    void Start()
    {
        life = 15;
        Timer = 5.0f;
        //boss = GameObject.FindGameObjectWithTag("Bigboss");
        ads = GetComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        enemys = GameObject.FindGameObjectsWithTag("enemy");
        boss = GameObject.FindGameObjectWithTag("Boss");
        bigboss = GameObject.FindGameObjectWithTag("Bigboss");
        life -= Time.deltaTime;
        //transform.Translate(0.05f, 0, 0);

        
        
        

        if (bigboss != null)
        {
            Vector3 ma = bigboss.transform.position - transform.position;
            Quaternion rota = Quaternion.LookRotation(ma);
            transform.rotation = Quaternion.Lerp(transform.rotation, rota, rotspeed);
            fsir = GameObject.Find("finalboss").GetComponent<finalboss>();
            if (fsir.GetComponent<BossHP>().hp<=0)
            {
                this.gameObject.SetActive(false);
            }

            //transform.Translate(0.05f, 0, 0);
            if (Timer > 0)
            {

                Timer -= Time.deltaTime;

            }
            else if (Timer <= 0 && life <= 10)
            {

                Instantiate(bullet, transform.position, transform.rotation);
                Timer = 2.0f;
                ads.PlayOneShot(fire);
            }
            if (life <= 0)
            {
                Destroy(Instantiate(exp, transform.position, Quaternion.identity), 0.2f);
                this.gameObject.SetActive(false);
                life = 15;
                //Destroy(gameObject);
            }

        }
        if (boss != null )
        {
            sir = GameObject.Find("boss").GetComponent<boss>();
            Vector3 ma = boss.transform.position - transform.position;
            Quaternion rota = Quaternion.LookRotation(ma);
            transform.rotation = Quaternion.Lerp(transform.rotation, rota, rotspeed);
            
            if (sir.GetComponent<BossHP>().hp <= 0)
            {
                this.gameObject.SetActive(false);

            }
            
            //transform.Translate(0.05f, 0, 0);
            if (Timer > 0)
            {

                Timer -= Time.deltaTime;

            }
            else if (Timer <= 0 && life <= 10)
            {

                Instantiate(bullet, transform.position, transform.rotation);
                Timer = 2.0f;
                ads.PlayOneShot(fire);
            }
            if (life <= 0)
            {
                Destroy(Instantiate(exp, transform.position, Quaternion.identity), 0.2f);
                this.gameObject.SetActive(false);
                life = 15;
                //Destroy(gameObject);
            }

        }
        if (enemys.Length != 0)
        {
            //if (dogboss.gameObject.activeInHierarchy)
            //{
            //    bigsir = GameObject.Find("bigboss").GetComponent<bigboss>();
            //    if (bigsir.GetComponent<EnemyHP>().hp <= 0)
            //    {
            //        this.gameObject.SetActive(false);

            //    }
            //}
            
            Vector3 md = enemys[0].transform.position - transform.position;
            Quaternion rot = Quaternion.LookRotation(md);
            transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
            if (Timer > 0)
            {

                Timer -= Time.deltaTime;

            }
            else if (Timer <= 0 && life <= 10)
            {

                Instantiate(bullet, transform.position, transform.rotation);
                Timer = 2.0f;
                ads.PlayOneShot(fire);
            }
            if (life <= 0)
            {
                Destroy(Instantiate(exp, transform.position, Quaternion.identity), 0.2f);
                this.gameObject.SetActive(false);
                life = 15;
                //Destroy(gameObject);
            }
        }
        


    }
}
