﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bigbossbullet : MonoBehaviour
{
    public GameObject target;
    public GameObject bullet;
    public float timer;
    public float rotspeed;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindWithTag("Player");
        timer = 1;
        rotspeed = 0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 md = target.transform.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(md);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
        timer -= Time.deltaTime;
        if(timer<=0)
        {
            Vector3 bulletDir = this.transform.position;
            Quaternion leftRota = Quaternion.AngleAxis(-10, Vector3.up);
            Quaternion RightRota = Quaternion.AngleAxis(10, Vector3.up);
            for (int i = 0; i < 1; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    switch (j)
                    {
                        case 0:
                            Instantiate(bullet, transform.position, transform.rotation);
                            break;
                        case 1:
                            bulletDir = RightRota * bulletDir;
                            Instantiate(bullet, transform.position, RightRota * transform.rotation);
                            break;
                        case 2:
                            bulletDir = leftRota * (leftRota * bulletDir);
                            Instantiate(bullet, transform.position, leftRota * transform.rotation);
                            bulletDir = RightRota * bulletDir;
                            break;
                    }
                }

            }
            Invoke("dead", 0.5f);
            timer = 5;
        }
    }
    void dead()
    {
        Destroy(gameObject);
    }
}
