﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public  class MainController : MonoBehaviour
{

    public static int GameState = 0;
    public static int GetNewWeaponTime=1;


    public int MissionState;
    //MainController.GameState

    public PlayableDirector sign_1;
    public PlayableDirector sign_2;
    public PlayableDirector sign_3;
    public PlayableDirector FinalSign;

    public PlayableDirector MenuToMission;
    public PlayableDirector MenuToReady;
    public PlayableDirector MissionToMenu;
    public PlayableDirector ReadyToMenu;




    public PlayableDirector AxeInfo;
    public PlayableDirector DogInfo;
   
    public PlayableDirector AxeBattleStart;
    public PlayableDirector DogBattleStart;
    public PlayableDirector AxeInfoBack;
    public PlayableDirector DogInfoBack;
    




    public GameObject DogButton;
    public GameObject BadDogButton;


    /// ///////////////////////////////////////////////////////////////////

    public PlayableDirector SignMusic_1;
    public PlayableDirector SignMusic_2;
    public PlayableDirector MenuMusic_1;
    public PlayableDirector FinalMusic;
    public PlayableDirector GetNewWeaponTimeLine;
    public PlayableDirector InfoTimeLineOpen;
    public PlayableDirector InfoTimeLineClose;

    public Image missile;

    void Start()
    {


        if (GameState == 0)
        {
            sign_1.Play();
            DogButton.SetActive(false);
            BadDogButton.SetActive(true);
            SignMusic_1.Play();
            missile.enabled = false;
        }
        else if (GameState == 1)
        {
            sign_3.Play();
            SignMusic_2.Play();
            //DogButton.SetActive(true);
            //BadDogButton.SetActive(false);
            missile.enabled = false;

        }
        else if (GameState == 2)
        {
            sign_3.Play();
            SignMusic_2.Play();
            DogButton.SetActive(true);
            BadDogButton.SetActive(false);
            missile.enabled = true;
            if (GetNewWeaponTime == 1)
            {
                GetNewWeaponTimeLine.Play();
                GetNewWeaponTime--;

            }


        }
        else if (GameState == 3) 
        {
            FinalSign.Play();
            FinalMusic.Play();
        }



    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(GameState);
        if (GameState == 0)
        {
            if (Input.GetKeyDown("o"))
            {
                sign_1.Stop();
                sign_2.Play();
                GameState++;
                SignMusic_1.Stop();
                MenuMusic_1.Play();
            }
        }
    }
    public void MainToMission_()
    {
        sign_3.Stop();
        sign_2.Stop();

        MissionToMenu.Stop();
        MenuToMission.Play();
        Debug.Log("mission");
    }


    public void MissionToMenu_() 
    {
        MenuToMission.Stop();
        MissionToMenu.Play();
        Debug.Log("menu");
        //Debug.Log("123");
    }

    public void AxeInfo_()
    {
        MenuToMission.Stop();
        AxeInfo.Play();
        
        Debug.Log("123");
    }

    public void DogInfo_()
    {
        MenuToMission.Stop();
        DogInfo.Play();

        Debug.Log("123");
    }


    public void AxeBattleStart_() 
    {
        AxeInfo.Stop();
        AxeBattleStart.Play();
        Invoke("Change_1", 8);
    }

    public void DogBattleStart_()
    {
        DogInfo.Stop();
        DogBattleStart.Play();
        Invoke("Change_2", 8);
    }

    public void AxeInfoBack_() 
    {
        AxeInfo.Stop();
        AxeInfoBack.Play();
    }

    public void DogInfoBack_()
    {
        DogInfo.Stop();
        DogInfoBack.Play();
    }


    public void MainToReady_()
    {
        sign_3.Stop();
        sign_2.Stop();

        ReadyToMenu.Stop();
        MissionToMenu.Stop();
        MenuToReady.Play();
    }

    public void ReadyToMenu_() 
    {
    MenuToReady.Stop();
    ReadyToMenu.Play();
    }


        void Change_1()
    {
        SceneManager.LoadScene(1);
    }


    void Change_2()
    {
        SceneManager.LoadScene(2);
    }

    public void End()
    {
        Application.Quit();
        Debug.Log(50);
    }
    public void InfoOpen()
    {
        InfoTimeLineClose.Stop();
        InfoTimeLineOpen.Play();
    }
    public void InfoClose()
    {
        InfoTimeLineOpen.Stop();
        InfoTimeLineClose.Play();
    }
}
