﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class chr1 : MonoBehaviour
{
    private Animator anim;
    MainController main;
    AudioSource AS;
    public GameObject Subtitle_1;


    public AudioClip Hello;

    void Start()
    {
        AS = GetComponent<AudioSource>();
        anim = gameObject.GetComponent<Animator>();
        main = GameObject.Find("Main").GetComponent<MainController>();
    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        if (Input.GetKeyDown("o"))
        {
            anim.SetTrigger("GameStart");
        } 
        else if (MainController.GameState != 0) 
        {
            anim.SetTrigger("GameStart");
        }        
    }
    void HelloVoice() 
    {
        AS.PlayOneShot(Hello);
    }
    private void FixedUpdate()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("hello2_vmd"))
        {
            Subtitle_1.SetActive(true);
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("HelloeToHips_vmd")) 
        {
            Subtitle_1.SetActive(false);
        }

    }

}
