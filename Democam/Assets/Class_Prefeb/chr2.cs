﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chr2 : MonoBehaviour
{
    private Animator anim;
    AudioSource AS;
    public AudioClip Hello;
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        AS = GetComponent<AudioSource>();
    }
    public void GearInot() 
    {
        anim.SetBool("GearInto",true);
        anim.SetBool("GearOut",false);
    }
    public void GearOut()
    {
        anim.SetBool("GearInto", false);
        anim.SetBool("GearOut", true);
    }

    void HelloVoice() 
    {
        AS.PlayOneShot(Hello);
    }

}
