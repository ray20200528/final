﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine.Rendering.PostProcessing;
using System.Runtime.CompilerServices;
//using UnityEditor.Profiling.Memory.Experimental;

public class CharacterController : MonoBehaviour
{
    public GameObject followtarget;
    public float SPEED;
    public float speed;
    public float mhp;
    public float EnemyDamage;
    public Image HpbarUI;
    public float PlayerHp;
    //public Text hp;
    public float stopmove = 1;
    public CharacterController cha;
    public bool losehp=false;
    private Animator anim;
    //public static Transform player;
    public bool power;
    amistate amistate;
    AudioSource AS;
    public float HurtTime;
    public int playdead;
    



    public GameObject SmallEngineRight;
    public GameObject SmallEngineLeft;
    public GameObject BigEngineRight;
    public GameObject BigEngineLeft;
    public bool Battle;
    Grain g;
    public float intensity;
    private float Damage;


    public PlayableDirector LoseTime;





    public AudioClip enigin;
    public AudioClip Hurt;
    public AudioClip warning;
    


    private void Awake() 
    {
        speed = SPEED;
    }
    private void Start()
    {
        HurtTime = 0;
        AS = GetComponent<AudioSource>();

        playdead = 0;
        PostProcessVolume ppv = GetComponent<PostProcessVolume>();
        PostProcessProfile ppp = ppv.profile;
        g = ppp.GetSetting<Grain>();
        //抓取後製元件
        power = false;
        //player = this.gameObject.transform;

        
        cha = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        amistate = GetComponent<amistate>();
        anim = GetComponent<Animator>();
        Battle = false;
        PlayerHp = mhp;
    }


    private void Update() 
    {
     
        g.intensity.value = intensity;
        intensity -= Time.deltaTime;
        //雜訊效果


        //print(PlayerHp);
        HpbarUI.fillAmount = PlayerHp / mhp;
        if (HurtTime != 0)
        {
            AS.PlayOneShot(Hurt);
            HurtTime--;
        }


        if (PlayerHp <= 0) 
        {
            LoseTime.Play();
            CancelInvoke();
            HpbarUI.color = Color.green;
            playdead = 0;
        }
        if(PlayerHp<=mhp*0.3f && playdead == 0 )
        {
            InvokeRepeating("deadwarn", 1, 1);
            HpbarUI.color = Color.red;
            playdead++;
        }
        
    }

    void deadwarn()
    {
        AS.PlayOneShot(warning);
        if (PlayerHp >= mhp * 0.3f)
        {
            HpbarUI.color = Color.green;
            CancelInvoke();
            playdead = 0;
        }
    }

    void OnTriggerEnter(Collider HitEffect)
    {
        //EnemyDamage ED;
        if (HitEffect.tag == "EnemyAttack")
        {
            //intensity = 1.0f;
            //ED = HitEffect.GetComponent<EnemyDamage>();
            //PlayerHp -= ED.Damage * amistate.PlayerDamageResist;
        }
    }

















    /// <summary>
    /// /////////////////////////////////////////////////////////////////////////////////////////////
    /// </summary>



    void SmallEnginineOpen() 
    {
        SmallEngineRight.SetActive(true);
        SmallEngineLeft.SetActive(true);
    }

    void SmallEnginineClose()
    {
        SmallEngineRight.SetActive(false);
        SmallEngineLeft.SetActive(false);
    }
    void BigEnginineOpen()
    {
        BigEngineRight.SetActive(true);
        BigEngineLeft.SetActive(true);
    }

    void BigEnginineClose()
    {
        BigEngineRight.SetActive(false);
        BigEngineLeft.SetActive(false);
    }
    private void LateUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        
        this.transform.forward = followtarget.transform.forward;
        if (Battle == true)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {

                SmallEngineRight.SetActive(false);
                SmallEngineLeft.SetActive(false);
                BigEngineRight.SetActive(true);
                BigEngineLeft.SetActive(true);

                AS.PlayOneShot(enigin);

                if (speed < 10f)
                {
                    speed = speed * 2f;
                }
            }
            else
            {
                SmallEngineRight.SetActive(true);
                SmallEngineLeft.SetActive(true);
                BigEngineRight.SetActive(false);
                BigEngineLeft.SetActive(false);



                speed = SPEED;
            }
        }
        //characterController.Move(new Vector3(horizontal, 0, vertical) *5.0f*Time.deltaTime);
        
        transform.position += followtarget.transform.right * horizontal * Time.deltaTime * speed*stopmove;
        transform.position += followtarget.transform.forward * vertical * Time.deltaTime * speed*stopmove;
       

    }
   




}


