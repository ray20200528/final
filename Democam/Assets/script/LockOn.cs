﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockOn : MonoBehaviour
{
    public class LockTarget
    {
        public GameObject obj;
        float halfWeight;

        public LockTarget(GameObject _obj, float _halfWeight)
        {
            obj = _obj;
            halfWeight = _halfWeight;
            //print(obj.name);
        }
        ~LockTarget()
        {
            //print("Unlock");
        }
    }
    public Transform playfire;
    public GameObject SightUI;//鎖定UI
    public GameObject EnemyHPUI;
    public float radius;
    public Transform player;
    public float smooth;
    public float ysmooth;
    private Vector3 smoothp = Vector3.zero;
    public float x, y, rs, fd;
    public Transform cameras;
    LayerMask enemylayer;
    //private GameObject lockTarget;
    public bool lockin;
    private int num, sec;
    public Vector3 cam;
    public Quaternion rot;
    public LockTarget lockTarget;
    public GameObject ft;
    public float lockdis;
    public bool LOCK;
    public Transform targets;
    public float rotspeed;
    public float BHP;
    public float MHP;
    public EnemyHP EnemyHP;
    private float HP;
    private float MAXHP;
    public Image HPUI;
    public Text EnemyDistanceUI;
    public BossHP bossblood;
    public BossHP bighp;
    public Image bosshp;
    public Image finalhp;
    AudioSource ads;
    public AudioClip blackhole;
    // Start is called before the first frame update
    void Start()
    {
        enemylayer = LayerMask.GetMask("enemylayer");
        //抓取鎖定目標上的EnemyHP元件及參數
        ads = GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        HPUI.fillAmount = HP / MAXHP;
        //UI顯示
        float fMX = Input.GetAxis("Mouse X");
        float fMY = Input.GetAxis("Mouse Y");
        x += fMX * rs;
        y -= fMY * rs;
        y = Mathf.Clamp(y, -40, 40);
        rot = Quaternion.Euler(y, x, 0);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, 0.1f);
        if (x > 360.0f)
        {
            x -= 360.0f;
        }
        else if (x < -360.0f)
        {
            x += 360.0f;
        }
        if (Checkw())
        {
            smoothp.y = Mathf.Lerp(transform.position.y, player.position.y + 0.8f, ysmooth * Time.deltaTime);
            smoothp.x = Mathf.Lerp(transform.position.x, player.position.x, smooth * Time.deltaTime);
            smoothp.z = Mathf.Lerp(transform.position.z, player.position.z, smooth * Time.deltaTime);
            transform.position = smoothp;
        }
        lockin = Input.GetMouseButtonDown(2);
        if (lockin)
        {
            enemy();
            //Debug.Log("move on");
        }

        if (lockTarget == null)
        {
            SightUI.SetActive(false);
            playfire.transform.forward = this.transform.forward;
            rs = 10;
        }
        else
        {
            Vector3 tempForwand = lockTarget.obj.transform.position - this.transform.position;
            lockdis = Vector3.Distance(lockTarget.obj.transform.position, player.position);
            //tempForwand.y = 0;
            this.transform.forward = tempForwand;
            playfire.transform.LookAt(lockTarget.obj.transform.position);
            
            Quaternion ma = Quaternion.LookRotation(lockTarget.obj.transform.position - this.transform.position);
            transform.rotation = Quaternion.Lerp(rot, ma, 0.4f);
            x = transform.eulerAngles.y;
            y = transform.eulerAngles.x;
            if(y >= 40)
            {
                y -= 360;
                if (y < -180)
                {
                    y = 40;
                }
            }
            if (y <= -40)
            {
                y += 360;
                if(y>=40)
                {
                    y = -40;
                }
            }
            rs = 0;
            EnemyHP = lockTarget.obj.GetComponent<EnemyHP>();
            bossblood = lockTarget.obj.GetComponent<BossHP>();
            bighp = lockTarget.obj.GetComponent<BossHP>();
            if (lockTarget.obj.tag == "enemy")
            {
                HP = EnemyHP.hp;
                MAXHP = EnemyHP.maxhp;//hpUI
                SightUI.SetActive(true);
                EnemyDistanceUI.text = lockdis.ToString("0.00");//距離ui
            }
            if (lockTarget.obj.tag == "Boss")
            {
                bossblood.enabled = true;
                bosshp.fillAmount = BHP / MHP;
                BHP = bossblood.hp;
                MHP = bossblood.maxhp;//hpUI
                EnemyDistanceUI.text = lockdis.ToString("0.00");//距離ui
            }
            if (lockTarget.obj.tag == "Bigboss")
            {
                HP = bighp.hp;
                MAXHP = bighp.maxhp;//hpUI
                finalhp.fillAmount = HP / MAXHP;
                EnemyDistanceUI.text = lockdis.ToString("0.00");//距離ui
            }
            Debug.DrawLine(playfire.position, lockTarget.obj.transform.position, Color.red);
            //player.transform.LookAt(lockTarget.obj.transform.position);
            if (lockTarget.obj.gameObject.activeInHierarchy == false)
            {
                lockTarget = null;
            }
            if (lockTarget.obj.GetComponentInChildren<SkinnedMeshRenderer>().enabled == false)
            {
                lockTarget = null;
            }
           
        }


    }

    bool Checkw()
    {
        return Vector3.Distance(transform.position, player.position) > radius;
    }
    private void OnDrawGizmos()
    {
        Debug.DrawLine(this.transform.position, this.transform.position + transform.forward * 30.0f, Color.green);
    }
    void enemy()
    {
        Vector3 boxCenter = player.position + new Vector3(0, 1.0f, 0) + player.forward * 500.0f;
        Collider[] cols = Physics.OverlapBox(boxCenter, new Vector3(5f, 5f, 500f), player.rotation, enemylayer);
        if (cols.Length != 0)
        {
            if (lockTarget == null || lockTarget.obj != cols[0].gameObject)
            {
                targets = cols[0].transform;
                ads.PlayOneShot(blackhole);
                foreach (var item in cols)
                {
                    lockTarget = new LockTarget(item.gameObject, item.bounds.extents.y);
                    //Debug.Log("lock");
                    
                    //LOCK = true;

                    break;
                }
            }
            else
            {
                lockTarget = null;
                //LOCK = false;
            }
        }
        else
        {
            lockTarget = null;
        }

    }
}
