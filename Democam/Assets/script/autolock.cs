﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class autolock : MonoBehaviour
{
    public class LockTarget
    {
        public GameObject obj;
        float halfWeight;

        public LockTarget(GameObject _obj, float _halfWeight)
        {
            obj = _obj;
            halfWeight = _halfWeight;
            print(obj.name);
        }
        ~LockTarget()
        {
            print("Unlock");
        }
    }
    public Transform playfire;
    public RawImage po;
    public float radius;
    public Transform player;
    public float smooth;
    public float ysmooth;
    private Vector3 smoothp = Vector3.zero;
    public float x, y, rs, fd;
    public Transform cameras;
    LayerMask enemylayer;
    //private GameObject lockTarget;
    public bool lockin;
    private int num, sec;
    public Vector3 cam;
    public Quaternion rot;
    private LockTarget lockTarget;
    public GameObject ft;
    public Transform Enemy;
    private void Awake()
    {
       
    }
    // Start is called before the first frame update
    void Start()
    {
        Enemy  = GameObject.FindGameObjectWithTag("Player").transform;


        enemylayer = LayerMask.GetMask("enemylayer");
    }
    // Update is called once per frame
    void Update()
    {
        float fMX = Input.GetAxis("Mouse X");
        float fMY = Input.GetAxis("Mouse Y");
        x += fMX * rs;
        y -= fMY * rs;
        y = Mathf.Clamp(y, -80, 80);
        rot = Quaternion.Euler(y, x, 0);
        transform.rotation = rot;
        if (x > 360.0f)
        {
            x -= 360.0f;
        }
        else if (x < -360.0f)
        {
            x += 360.0f;
        }
        if (Checkw())
        {
            smoothp.y = Mathf.Lerp(transform.position.y, player.position.y + 1f, ysmooth * Time.deltaTime);
            smoothp.x = Mathf.Lerp(transform.position.x, player.position.x, smooth * Time.deltaTime);
            smoothp.z = Mathf.Lerp(transform.position.z, player.position.z, smooth * Time.deltaTime);
            transform.position = smoothp;
        }
        Vector3 md = Enemy.position - transform.position;
        Quaternion rat = Quaternion.LookRotation(md);
        transform.rotation = rat;
        //lockin = Input.GetMouseButtonDown(1);
        if (lockin)
        {
            enemy();
            Debug.Log("move on");
        }
        if (lockTarget == null)
        {
            Vector3 tempModelEuler = this.transform.eulerAngles;
            this.transform.eulerAngles = tempModelEuler;
            po.enabled = false;

        }
        else
        {
            Vector3 tempForwand = lockTarget.obj.transform.position - player.transform.position;
            tempForwand.y = 0;
            player.transform.forward = tempForwand;
            //this.transform.LookAt(lockTarget.obj.transform);
            myLookAt(lockTarget.obj.transform.position, this.transform.position);
            po.enabled = true;
            Debug.DrawLine(playfire.position, lockTarget.obj.transform.position, Color.red);
            playfire.transform.LookAt(lockTarget.obj.transform.position);

        }


    }
    bool Checkw()
    {
        return Vector3.Distance(transform.position, player.position) > radius;
    }
    private void OnDrawGizmos()
    {
        Debug.DrawLine(this.transform.position, this.transform.position + transform.forward * 30.0f, Color.green);
    }
    void myLookAt(Vector3 form, Vector3 to)
    {
        transform.rotation = Quaternion.LookRotation(form - to);
    }
    void enemy()
    {
        Vector3 boxCenter = player.position + new Vector3(0, 1.0f, 0) + player.forward * 5.0f;
        Collider[] cols = Physics.OverlapBox(boxCenter, new Vector3(5f, 5f, 10000000f), player.rotation, enemylayer);
        if (cols.Length != 0)
        {
            if (lockTarget == null || lockTarget.obj != cols[0].gameObject)
            {
                foreach (var item in cols)
                {
                    lockTarget = new LockTarget(item.gameObject, item.bounds.extents.y);
                    Debug.Log("lock");

                    break;
                }
            }
            else
            {
                lockTarget = null;
            }
        }
        else
        {
            lockTarget = null;
        }

    }
}
