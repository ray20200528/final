﻿using UnityEngine;
using UnityEditor;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
public class Character : MonoBehaviour
{
    public GameObject followtarget;
    public float SPEED;
    private float speed;
    public int maxhp = 1000;
    public int curhp;
    public int mhp = 1000;
    public Text hp;
    public float x;
    public Transform cub;

    private void Awake()
    {
        speed = SPEED;
        curhp = maxhp;
    }
    private void Start()
    {
        
    }

    private void Update()
    {
        print(speed);

    }
    void Hpstate(int hps)
    {
        curhp += hps;
        if (curhp <= 0)
        {
            //this.gameObject.SetActive(false);
            print("You Lose!!!");
        }
    }
    private void LateUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");


        this.transform.forward = followtarget.transform.forward;
        
        if (Input.GetKey(KeyCode.Space))
        {
            if (speed < 10f)
            {
                speed = speed * 1.2f;
            }
        }
        else
        { speed = SPEED; }
        //characterController.Move(new Vector3(horizontal, 0, vertical) *5.0f*Time.deltaTime);
        transform.position += followtarget.transform.right * horizontal * Time.deltaTime * speed;
        transform.position += followtarget.transform.forward * vertical * Time.deltaTime * speed;


    }
    void OnTriggerEnter(Collider bu)
    {

        if (bu.tag == "enemy")
        {
            Hpstate(-50);
            print("boom");
            mhp -= 50;
            hp.text = mhp.ToString();
        }
    }
}
