﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI3d : MonoBehaviour
{
    private float distanceToMe;           //智能体到目标的距离
    public GameObject target;                //目标角色
    private float isSeekDistance = 100.0f;  //可靠近范围
    public int state;                     //智能体状态
    private Animator ani;
    private int maxhp = 200;
    public int curhp;
    
    void Start()
    {
        target = GameObject.FindWithTag("Player");
        ani = GetComponent<Animator>();
        curhp = maxhp;
        
    }


    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case 0:
                Idle();   //空闲，四处游荡
                break;
            case 1:
                Seek();   //向目标靠近
                break;
        }

    }
    void Idle()
    {

        distanceToMe = Vector3.Distance(target.transform.position, this.transform.position);
        if (distanceToMe > isSeekDistance) //大于可靠近范围，进入空闲状态
        {
            state = 0;
            
            //if (Random.value > 0.5)             //通过随机值，使其随机左右移动
            //{
            //    this.transform.Rotate(Vector3.up * 5);
            //}
            //else
            //{
            //    transform.Rotate(Vector3.up * -5.0f);
            //}
            //this.transform.Translate(Vector3.forward * 0.1f);
        }
        else
        {
            state = 1;
        }
    }
    void Seek()
    {

        distanceToMe = Vector3.Distance(target.transform.position, this.transform.position);
        if (distanceToMe < isSeekDistance)
        {
            this.transform.LookAt(target.transform);               //该方法使智能体总是面对目标
            this.transform.Translate(Vector3.forward * 0.06f);  //向目标前进，即靠近（Vector3.back 后退，则逃避）
            if (distanceToMe <= 10f)
            {
                this.transform.Translate(Vector3.forward * -0.01f);
                print("close");
                

            }
            if (distanceToMe <= 3f)
            {              
                this.transform.Translate(Vector3.forward * -0.05f);
                
                ani.SetBool("bladeattack", true);

            }else
            {
                ani.SetBool("bladeattack", false);
            }
        }
        else
        {
            state = 0;
        }
    }
    private void OnDrawGizmos()
    {
        Debug.DrawLine(this.transform.position, this.transform.position + transform.forward * 20.0f, Color.red);
    }
    void Hpstate(int hps)
    {
        curhp += hps;
        
        if(curhp == 100)
        {
            ani.SetBool("hurt", true);
        }else
        {
            ani.SetBool("hurt", false);
        }
        if (curhp <= 0)
        {
            ani.SetBool("dead", true);
            Invoke("Dead", 1.5f);
            //this.gameObject.SetActive(false);
        }

    }
    void OnTriggerEnter(Collider bu)
    {
        print("shoot");
        if (bu.tag == "bullet")
        {
            Hpstate(-50);
            
        }
    }
   void Dead()
    {
        this.gameObject.SetActive(false);
    }
}
