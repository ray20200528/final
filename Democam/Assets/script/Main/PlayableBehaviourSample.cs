﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

[System.Serializable]
public class PlayableBehaviourSample : PlayableAsset
{
    //使用ExposedReference進行賦值操作
    public ExposedReference<GameObject> ShowNumberText;

    private Text text;

    public int startNum;


    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var scriptPlayable = ScriptPlayable<PlayableTest>.Create(graph);
        //從ExposedReference中獲取我們需要的控件
        text = ShowNumberText.Resolve(graph.GetResolver()).GetComponent<Text>();
        //對指定的PlayableBehaviour中的屬性進行賦值
        scriptPlayable.GetBehaviour().ShowNumberText = text;
        scriptPlayable.GetBehaviour().StartNum = startNum;
        return scriptPlayable;
    }
}