﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    AudioSource AS;
    public AudioClip SpecialShootVoice;
    public AudioClip MissionVoice;
    public AudioClip Slash2Voice;
    public AudioClip Slash1Voice;
    public AudioClip DefVoice;
    public AudioClip RollVoice;
    public AudioClip HurtVoice;
    public AudioClip LeaserVoice;
    public AudioClip CounterVoice;

    public AudioClip SpecialShoot;
    public AudioClip Mission;
    public AudioClip Leaser;
    // Start is called before the first frame update
    void Start()
    {
        AS = GetComponent<AudioSource>();
    }

    public void SpecialShoot_() 
    {
        AS.PlayOneShot(SpecialShoot);
    }
    public void Mission_()
    {
        AS.PlayOneShot(Mission);
    }

    public void Leaser_()
    {
        AS.PlayOneShot(Leaser);
    }

    public void SpecialShootVoice_() 
    {
        AS.PlayOneShot(SpecialShoot);
    }

    public void MissionVoice_()
    {
        AS.PlayOneShot(MissionVoice);
    }

    public void Slash2Voice_()
    {
        AS.PlayOneShot(Slash2Voice);
    }

    public void DefVoice_()
    {
        AS.PlayOneShot(DefVoice);
    }

    public void RollVoice_()
    {
        AS.PlayOneShot(RollVoice);
    }

    public void HurtVoice_()
    {
        AS.PlayOneShot(HurtVoice);
    }

    public void LeaserVoice_()
    {
        AS.PlayOneShot(LeaserVoice);
    }

    public void Slash1Voice_()
    {
        AS.PlayOneShot(Slash1Voice);
    }

    public void CounterVoice_()
    {
        AS.PlayOneShot(CounterVoice);
    }
}
