﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleFiled : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject BattleFiledWall;
    public Text num;
    public bool outside;
    public float timer = 10;
    CharacterController chr;
    private void Start()
    {
        num.enabled = false;
        outside = false;
        timer = 10;
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
    }
    private void Update()
    {
        //num.text = timer + "aaa";
        if (outside)
        {
            num.text = timer + "";
            //num.text = timer.ToString();
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                timer = 30;
                chr.PlayerHp = 0;
                num.text = "";
                //num.enabled = false;
            }
        }
        

    }
    void OnTriggerStay(Collider BattleFiled)
    {
        if (BattleFiled.tag == "Player")
        {
            BattleFiledWall.SetActive(true);
            num.enabled = true;
            outside = true;
        }
    }

    void OnTriggerExit(Collider BattleFiled)
    {
        if (BattleFiled.tag == "Player")
        {
            BattleFiledWall.SetActive(false);
            num.enabled = false;
            outside = false;
            timer = 10;
        }
    }
    // Update is called once per frame

}
