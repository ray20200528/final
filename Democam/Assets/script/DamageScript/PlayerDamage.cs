﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerDamage : MonoBehaviour
{
    public float Damage;
    public bool dg;
    public boss bosshp;
    public finalboss bighp;
    ami ami;
    
    private void Start()
    {
        ami = GameObject.Find("一条綾香").GetComponent<ami>();
        
    }
    void OnTriggerEnter(Collider DamageEvent)
    {

        if (DamageEvent.tag == "enemy")
        {
           
            EnemyHP EnemyHP;
            EnemyHP = DamageEvent.gameObject.GetComponent<EnemyHP>();
            EnemyHP.hp -= Damage;
            UImain.m_Instance.SpawnFloatingText(DamageEvent.transform.position, Damage.ToString());
            Destroy(gameObject);

            


            ami.SPpower += 1;//大招計量
        }
        if (DamageEvent.tag == "Boss")
        {
            bosshp = GameObject.Find("boss").GetComponent<boss>();
            BossHP bossHP;
            bossHP = DamageEvent.gameObject.GetComponent<BossHP>();
            bossHP.hp -= Damage * bosshp.lockblood;
            UImain.m_Instance.SpawnFloatingText(DamageEvent.transform.position, (Damage * bosshp.lockblood).ToString());
            Destroy(gameObject);

            ami.SPpower += 1;//大招計量
        }
        if (DamageEvent.tag == "Bigboss")
        {
            bighp = GameObject.Find("finalboss").GetComponent<finalboss>();
            BossHP bossHP;
            bossHP = DamageEvent.gameObject.GetComponent<BossHP>();
            bossHP.hp -= Damage * bighp.lockblood;
            UImain.m_Instance.SpawnFloatingText(DamageEvent.transform.position, (Damage * bighp.lockblood).ToString());
            Destroy(gameObject);

            ami.SPpower += 1;//大招計量
        }
    }
}
