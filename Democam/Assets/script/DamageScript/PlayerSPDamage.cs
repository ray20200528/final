﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSPDamage : MonoBehaviour
{
    public float LaserDamage;
    public bool Laser;
    public boss bosshp;
    public finalboss bighp;
    public GameObject SPExplosion;
    private void Start()
    {

    }
    void OnTriggerEnter(Collider LaserDamageEvent)
    {


        if (LaserDamageEvent.tag == "enemy")
        {
            EnemyHP EnemyHP;
            EnemyHP = LaserDamageEvent.gameObject.GetComponent<EnemyHP>();
            EnemyHP.hp -= LaserDamage;
            UImain.m_Instance.SpawnFloatingText(LaserDamageEvent.transform.position, LaserDamage.ToString());
            ScreenShake.isshakeCamera = true;//畫面震動效果
            Destroy(Instantiate(SPExplosion, LaserDamageEvent.transform.position, LaserDamageEvent.transform.rotation), 0.8f);

        }
        if (LaserDamageEvent.tag == "Boss")
        {
            bosshp = GameObject.Find("boss").GetComponent<boss>();
            BossHP EnemyHP;
            EnemyHP = LaserDamageEvent.gameObject.GetComponent<BossHP>();
            EnemyHP.hp -= LaserDamage * bosshp.lockblood;
            UImain.m_Instance.SpawnFloatingText(LaserDamageEvent.transform.position, (LaserDamage * bosshp.lockblood).ToString());
            ScreenShake.isshakeCamera = true;//畫面震動效果
            Destroy(Instantiate(SPExplosion, LaserDamageEvent.transform.position, LaserDamageEvent.transform.rotation), 0.8f);

        }
        if (LaserDamageEvent.tag == "Bigboss")
        {
            bighp = GameObject.Find("finalboss").GetComponent<finalboss>();
            BossHP bossHP;
            bossHP = LaserDamageEvent.gameObject.GetComponent<BossHP>();
            bossHP.hp -= LaserDamage * bighp.lockblood;
            UImain.m_Instance.SpawnFloatingText(LaserDamageEvent.transform.position, (LaserDamage * bighp.lockblood).ToString());
            ScreenShake.isshakeCamera = true;//畫面震動效果  
            Destroy(Instantiate(SPExplosion, LaserDamageEvent.transform.position, LaserDamageEvent.transform.rotation), 0.8f);

        }
    }
}
