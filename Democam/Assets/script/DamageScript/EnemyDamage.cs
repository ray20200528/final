﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;


public class EnemyDamage : MonoBehaviour
{
    public float Damage;
    public float Resist;
    public amistate cha;
    public CharacterController cc;
    Grain g;
    private float v;

    public bool Hurt;


    //public Image blood;
    public GameObject player;
    private void Start()
    {
        cha = GameObject.Find("一条綾香").GetComponent<amistate>();
        cc = GameObject.Find("一条綾香").GetComponent<CharacterController>();
    }



    void OnTriggerEnter(Collider DamageEvent)
    {
        if (DamageEvent.tag == "Player")
        {
            CharacterController chr;
            chr = DamageEvent.gameObject.GetComponent<CharacterController>();
            chr.PlayerHp -= Damage*cha.PlayerDamageResist;
            UImain.m_Instance.SpawnPlayerText(DamageEvent.transform.position, (Damage * cha.PlayerDamageResist).ToString());
            if (Damage * cha.PlayerDamageResist >= 500)
            {                             
                chr.intensity = 1.0f;
                chr.HurtTime++;
            }



            if (Damage * cha.PlayerDamageResist >= 50) 
            {
                ScreenShake.isshakeCamera = true;
            }
            Destroy(gameObject);
        }
    }
}
