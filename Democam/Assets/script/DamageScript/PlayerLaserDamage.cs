﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLaserDamage : MonoBehaviour
{
    public float LaserDamage;
    AudioSource AS;
    public AudioClip LaserShootSound;
    public bool Laser;
    public boss bosshp;
    public finalboss bighp;
    private void Start()
    {
        AS = GetComponent<AudioSource>();
        AS.PlayOneShot(LaserShootSound);
    }
    void OnTriggerEnter(Collider LaserDamageEvent)
    {


        if (LaserDamageEvent.tag == "enemy")
        {
            EnemyHP EnemyHP;
            EnemyHP = LaserDamageEvent.gameObject.GetComponent<EnemyHP>();
            EnemyHP.hp -= LaserDamage;
            UImain.m_Instance.SpawnFloatingText(LaserDamageEvent.transform.position, LaserDamage.ToString());
            ScreenShake.isshakeCamera = true;//畫面震動效果
            ami.SPpower += 5;//大招計量
        }
        if (LaserDamageEvent.tag == "Boss")
        {
            bosshp = GameObject.Find("boss").GetComponent<boss>();
            BossHP EnemyHP;
            EnemyHP = LaserDamageEvent.gameObject.GetComponent<BossHP>();
            EnemyHP.hp -= LaserDamage * bosshp.lockblood;
            UImain.m_Instance.SpawnFloatingText(LaserDamageEvent.transform.position, (LaserDamage * bosshp.lockblood).ToString());
            ScreenShake.isshakeCamera = true;//畫面震動效果
            ami.SPpower += 5;//大招計量
        }
        if (LaserDamageEvent.tag == "Bigboss")
        {
            bighp = GameObject.Find("finalboss").GetComponent<finalboss>();
            BossHP bossHP;
            bossHP = LaserDamageEvent.gameObject.GetComponent<BossHP>();
            bossHP.hp -= LaserDamage * bighp.lockblood;
            UImain.m_Instance.SpawnFloatingText(LaserDamageEvent.transform.position, (LaserDamage * bighp.lockblood).ToString());
            ScreenShake.isshakeCamera = true;//畫面震動效果  
            ami.SPpower += 5;//大招計量
        }
    }
}
