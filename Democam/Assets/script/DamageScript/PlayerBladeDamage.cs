﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBladeDamage : MonoBehaviour
{
    public float BladeDamage;
    public bool Blade;
    public boss bosshp;
    public GameObject BladeHit;
    public finalboss bighp;


    public AudioClip cut;
    public AudioClip slash;
    AudioSource ads;


    private void Start()
    {
        ads = GetComponent<AudioSource>();
        ads.PlayOneShot(slash);      
    }
    void OnTriggerEnter(Collider BladeDamageEvent)
    {

        if (BladeDamageEvent.tag == "enemy")
        {
            ads.PlayOneShot(cut);

            EnemyHP EnemyHP;

            EnemyHP = BladeDamageEvent.gameObject.GetComponent<EnemyHP>();

            EnemyHP.hp -= BladeDamage;
            UImain.m_Instance.SpawnFloatingText(BladeDamageEvent.transform.position, BladeDamage.ToString());


            Destroy(Instantiate(BladeHit, BladeDamageEvent.transform.position, BladeDamageEvent.transform.rotation), 0.8f);//斬擊效果
            ScreenShake.isshakeCamera = true;//畫面震動效果   
            ami.SPpower += 2.5f;//大招計量
        }
        if (BladeDamageEvent.tag == "Boss")
        {
            ads.PlayOneShot(cut);

            bosshp = GameObject.Find("boss").GetComponent<boss>();
            BossHP EnemyHP;
            EnemyHP = BladeDamageEvent.gameObject.GetComponent<BossHP>();
            EnemyHP.hp -= BladeDamage * bosshp.lockblood;

            UImain.m_Instance.SpawnFloatingText(BladeDamageEvent.transform.position, (BladeDamage * bosshp.lockblood).ToString());

            Destroy(Instantiate(BladeHit, BladeDamageEvent.transform.position, BladeDamageEvent.transform.rotation), 0.8f);//斬擊效果
            ScreenShake.isshakeCamera = true;//畫面震動效果
            ami.SPpower += 2.5f;//大招計量
        }
        if (BladeDamageEvent.tag == "Bigboss")
        {
            ads.PlayOneShot(cut);

            bighp = GameObject.Find("finalboss").GetComponent<finalboss>();
            BossHP bossHP;
            bossHP = BladeDamageEvent.gameObject.GetComponent<BossHP>();
            bossHP.hp -= BladeDamage * bighp.lockblood;


            UImain.m_Instance.SpawnFloatingText(BladeDamageEvent.transform.position, (BladeDamage * bighp.lockblood).ToString());
            Destroy(Instantiate(BladeHit, BladeDamageEvent.transform.position, BladeDamageEvent.transform.rotation), 0.8f);//斬擊效果
            ScreenShake.isshakeCamera = true;//畫面震動效果 
            ami.SPpower += 2.5f;//大招計量
        }
    }
}

