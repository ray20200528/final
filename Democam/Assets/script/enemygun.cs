﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemygun : MonoBehaviour
{
    public float Timer;
    public GameObject bullet;

    // Start is called before the first frame update
    void Start()
    {
        Timer = 2.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(Timer >=0)
        {
            Timer -= Time.deltaTime;
        }
        else if (Timer <= 0)
        {
            Instantiate(bullet, transform.position, transform.rotation);
            Timer = 2.0f;
        }
    }
    void OnTriggerEnter(Collider bu)
    {
        if (bu.tag == "bullet")
        {
          
        }
    }
}
