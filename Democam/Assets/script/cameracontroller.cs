﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameracontroller : MonoBehaviour
{
    public float radius;
    public Transform player;
    public float smooth;
    public float ysmooth;
    private Vector3 smoothp = Vector3.zero;
    public Transform targget;
    private bool Lock;
    private bool xxx = false;
    private void Awake()
    {
        targget = transform.Find("/Cube").GetComponent<Transform>();
    }
    // Start is called before the first frame update
    void Start()
    {
        if (!player)
        {
            this.enabled = false;
        }
        else
        {
            transform.parent = null;
        }


    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Checkw())
        {
            smoothp.y = Mathf.Lerp(transform.position.y, player.position.y, ysmooth * Time.deltaTime);
            smoothp.x = Mathf.Lerp(transform.position.x, player.position.x, smooth * Time.deltaTime);
            smoothp.z = Mathf.Lerp(transform.position.z, player.position.z, smooth * Time.deltaTime);
            transform.position = smoothp;
        }
        if (Input.GetMouseButton(0))
        {
            if (xxx != true)
            {

                Lock = true;
            }

        }

        if (Lock == true)
        {
            //将Cube_1的z轴指向坐标原点（Vector.zero）到 Vector3（2，2，2）所对应的向量方向
            //transform.rotation = Quaternion.LookRotation(new Vector3(2, 2, 2));
            //将Cube_1的z轴指向原点到other.position对应向量的方向
            //transform.rotation = Quaternion.LookRotation(targget.position- transform.position);
            myLookAt(targget.position, this.transform.position);
        }
        if (Input.GetMouseButtonDown(1))
        {
            Lock = false;
            transform.forward = player.forward;
        }

        Debug.DrawLine(transform.position, targget.position, Color.blue);

        Debug.DrawRay(transform.position, targget.position, Color.red);


    }
    bool Checkw()
    {
        return Vector3.Distance(transform.position, player.position) > radius;
    }
    private void OnDrawGizmos()
    {
        Debug.DrawLine(this.transform.position, this.transform.position + transform.forward * 2.0f, Color.green);
    }
    void myLookAt(Vector3 form, Vector3 to)
    {
        transform.rotation = Quaternion.LookRotation(form - to);
    }
}
