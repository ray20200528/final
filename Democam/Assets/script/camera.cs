﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera : MonoBehaviour
{
    public Transform lookTarget;
    public float followDistance;
    private Vector3 cam;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void LateUpdate()
    {
        
        cam = lookTarget.rotation * new Vector3(0, 0, -followDistance) + lookTarget.position;
        transform.position = cam;
        transform.forward = lookTarget.forward;
        transform.rotation = Quaternion.Lerp(transform.rotation, lookTarget.rotation, 0.1f);
    }
}