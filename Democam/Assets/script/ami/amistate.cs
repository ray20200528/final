﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class amistate : MonoBehaviour
{
    public float PowerUpTime;
    private float RushDistance;
    private float LimitDistance = 5;
    private float SafeDistance;
    private float RealRush;
    private Cinemachine.CinemachineCollisionImpulseSource MyInpulse;
    AudioSource AS;
    private AnimatorStateInfo currentBaseState;
    static int idleState = Animator.StringToHash("Base Layer.attack1-3");
    private Animator anim;
    public GameObject JumpNum;
    public GameObject blade;
    public GameObject FireBlade;
    public GameObject shield;
    public GameObject gun;
    public GameObject bullet;
    public GameObject SPWeapon;
    public playfire bulletcontroller;//設定bulletcontroller為準備從playfire那拿資料的空object
    public CharacterController cha;
    public ami ami;
    public LockOn LockData;
    public float movespeed;
    public LockOn LockTarget;
    public ami gunactive;
    private float movedis;
    public Image dead;
    public Text blood;
    public RawImage spot;
    public float PlayerDamageResist;
    public PlayableDirector LoseTime;
    public float FireBlade_4_Move;
    //public GameObject Slash1;
    public GameObject Slash1;
    public GameObject Slash2;
    public GameObject Slash3;
    public GameObject HitBox1;
    public GameObject HitBox2;
    public GameObject HitBox3;
    public GameObject SpecialShootBox;
    public GameObject LaserBox;
    public GameObject LaserBox2;
    public GameObject SpBox1;
    public GameObject SpBox2;
    public GameObject FireBladeBox_1;
    public GameObject FireBladeBox_2;
    public GameObject FireBladeBox_3;
    public GameObject FireBladeBox_4;
    public GameObject FireBox_1;
    public GameObject FireBox_2;
    public GameObject FireBox_3;
    public GameObject FireBox_4;
    public GameObject DuelAttackBox;
    public GameObject PowerUpBox;
    public GameObject FireBladeWaveBox;


   

    public GameObject counter1;
    public GameObject counter2;
    public GameObject counterBird;
    public GameObject counterslash_1_1;
    public GameObject counterslash_1_2;
    public GameObject counterslash_1_3;
    public GameObject counterslash_2_1;
    public GameObject counterslash_2_2;
    public GameObject counterslash_Bird;
    public GameObject SpecialShootmuzzlePrefab_2;
    public GameObject SpecialShootReadyPrefab;
    public GameObject LaserReady;
    public GameObject LaserShoot;
    public GameObject LaserWave;
    public GameObject FireBladeSlash_1;
    public GameObject FireBladeSlash_2;
    public GameObject FireBladeSlash_3;
    public GameObject FireBladeSlash_4;
    public GameObject FireBladeFire_1;
    public GameObject FireBladeFire_2;
    public GameObject FireBladeFire_3;
    public GameObject FireBladeFire_4;
    public GameObject RedPartical_1;
    public GameObject RedPartical_2;
    public GameObject RedPartical_3;
    public GameObject RedPartical_4;
    //public GameObject FireBladePartic_1;
    //public GameObject FireBladePartic_2;
    //public GameObject FireBladePartic_3;
    //public GameObject FireBladePartic_4;
    public GameObject DuelAttack;
    public GameObject FireBladeSlash_Wave_1;
    public GameObject FireBladeSlash_Wave_2;
    public GameObject FireBladeSlash_Wave_3;
    public GameObject FireBladeSlash_Wave_4;
    public GameObject FireWalk;
    public GameObject DuelAttackWave;
    public GameObject PowerUp;
    public GameObject fblood;











    public GameObject SPMuzzle1;
    public GameObject SPMuzzle2;
    public GameObject SPShoot;
    public GameObject SPWave;







    public GameObject Boss_1_UI;
    public GameObject FinalBoss_UI;
    public GameObject battleUI;
    public GameObject loseUI;
    public GameObject PlayerUI;


    public GameObject MainCamera;
    public GameObject LoseCamera;


    public PlayableDirector LoseTimeLine;
    public PlayableDirector ReturnTimeLine;
    public PlayableDirector BattleMusic;


    public AudioClip PowerUpSoundEff;
    public AudioClip DuelAttackVoice;
    public AudioClip PowerUpVoice;
    public AudioClip FireBladeAttackVoice;
    public AudioClip FireBladeAttackSlashSoundEff;








    void Start()
    {
        AS = GetComponent<AudioSource>();


        PlayerDamageResist = 1;
        anim = GetComponent<Animator>();

        cha = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        cha.stopmove = 1;

        gunactive = GameObject.Find("一条綾香").GetComponent<ami>();
        gunactive.gunactive = true;

        ami= GameObject.Find("一条綾香").GetComponent<ami>();
        SPMuzzle1.SetActive(false);


        bulletcontroller = GameObject.Find("fire").GetComponent<playfire>();

        LockData = GameObject.Find("lock").GetComponent<LockOn>();//現在把LockOn中的lock物件帶入bulletcontroller
        //LockData.smooth = 4;

        MyInpulse = GetComponent<Cinemachine.CinemachineCollisionImpulseSource>();
    }

    private void Update()
    {
        if (PowerUpTime > 0)
        {
            FireWalk.SetActive(true);
        }
        else 
        {
            FireWalk.SetActive(false);
        }



        if (LockData.lockTarget != null)
        {
            SafeDistance = LockData.lockdis - LimitDistance;
            if (SafeDistance < RushDistance)
            {
                RealRush = SafeDistance;
            }
            else
            {
                RealRush = RushDistance;
            }
        }
        else 
        {
            RealRush = RushDistance;
        }
 
        if (LockData.lockTarget == null)
        {
            movedis = 1;
        }
        else
        {
            if (LockData.lockdis <= LimitDistance)
            {
                movedis = 0;
            }
            else
            {
                movedis = 1;
            }
        }




        if (anim.GetCurrentAnimatorStateInfo(0).IsName("lose_2"))
        {
            gunactive.gunactive = false;
            blade.SetActive(true);
            shield.SetActive(true);
            gun.SetActive(false);
            bulletcontroller.fire = false;
            cha.stopmove = 0;
            LockData.smooth = 15;
            fblood.SetActive(false);
            battleUI.SetActive(false);
            PlayerUI.SetActive(false);


            if (Input.GetKeyDown("o"))
            {
                loseUI.SetActive(false);
                battleUI.SetActive(true);
                PlayerUI.SetActive(true);
                cha.playdead = 0;
                LoseTime.Stop();



                LoseCamera.SetActive(false);

                anim.SetBool("lose", false);



                cha.PlayerHp = cha.mhp;
                cha.HpbarUI.fillAmount = 1;
            }
            if (Input.GetKeyDown(KeyCode.P))
            {
                ReturnTimeLine.Play();

                BattleMusic.Stop();

                Invoke("Return", 5);
            }
        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("Duel_4_vmd"))
        {
            PlayerDamageResist = 0.1f;
            if (Input.GetMouseButtonDown(1))
            {
                anim.SetBool("DuelAttack", true);
            }
        }



    }



    private void FixedUpdate()
    {            

       
       

        currentBaseState = anim.GetCurrentAnimatorStateInfo(0);


        if (anim.GetCurrentAnimatorStateInfo(0).IsName("start_1-10_vmd"))
        {
            gunactive.gunactive = false;
            bulletcontroller.fire = false;
            LockData.y = 0;
            LockData.x = 0;
            cha.stopmove = 0;            
        }


        if (anim.GetCurrentAnimatorStateInfo(0).IsName("sp1-6_vmd"))
        {
            FireBlade.SetActive(false);
            gunactive.gunactive = false;
            blade.SetActive(false);
            shield.SetActive(false);
            gun.SetActive(false);
            SPWeapon.SetActive(true);
            //bullet.SetActive(false);
            bulletcontroller.fire = false;
            cha.stopmove = 0;
            PlayerDamageResist = 0;
            cha.power = true;
            
        }



        if (anim.GetCurrentAnimatorStateInfo(0).IsName("readypose"))
        {
            gunactive.gunactive = false;
            blade.SetActive(true);
            shield.SetActive(true);
            gun.SetActive(false);
            //bullet.SetActive(false);
            bulletcontroller.fire = false;
            cha.stopmove = 0;
            LockData.radius = 0.1f;
            LockData.smooth = 2;

        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("move"))
        {
            JumpNum.SetActive(true);
            blade.SetActive(false);
            shield.SetActive(false);
            FireBlade.SetActive(false);
            gun.SetActive(true);
            cha.Battle = true;
            cha.stopmove = 1;
            bulletcontroller.fire = true;
            gunactive.gunactive = true;
            loseUI.SetActive(false);
            battleUI.SetActive(true);
            PlayerUI.SetActive(true);
            fblood.SetActive(true);
            MainCamera.SetActive(true);
            if (Boss_1_UI != null)
            {
                Boss_1_UI.SetActive(true);
            }

            if (FinalBoss_UI != null)
            {
                FinalBoss_UI.SetActive(true);
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////
        ///



        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("FireReadypose_vmd"))
        {
            if (PowerUpTime>0) 
            {
                PlayerDamageResist = 0.5f;
            }
            gunactive.gunactive = false;
            FireBlade.SetActive(true);
            shield.SetActive(true);
            gun.SetActive(false);
            bulletcontroller.fire = false;
            cha.stopmove = 0;
            LockData.radius = 0.1f;
            LockData.smooth = 2;
            RushDistance = 8f;

        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("FireBattle1-1_2_vmd"))
        {
            RushDistance = 8f;
            cha.stopmove = 0;
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("FireBattle1-2_2_vmd"))
        {
            if (Input.GetMouseButton(1))
            {
                anim.SetBool("FireAttack_2", true);
            }
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("FireBattle2-1_2_vmd"))
        {
            RushDistance = 7f;
            cha.stopmove = 0;
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("FireBattle2-2_2_vmd"))
        {
            if (Input.GetMouseButton(1))
            {
                anim.SetBool("FireAttack_3", true);
            }
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("FireBattle3-1_2_vmd"))
        {
            RushDistance = 7f;
            cha.stopmove = 0;
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("FireBattle3-2_2_vmd"))
        {
            if (Input.GetMouseButton(1))
            {
                anim.SetBool("FireAttack_4", true);
            }
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("FireBattle4-1_2_vmd"))
        {
            RushDistance = 9f;
            cha.stopmove = 0;
        }

        //else if (anim.GetCurrentAnimatorStateInfo(0).IsName("FireBattle4-1_2_vmd"))
        //{
        //    transform.Translate(0, 0, 1.5f * movedis);
        //    cha.stopmove = 0;
        //}


        
        
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("DuelAttack_2_vmd"))
        {
            if (LockData.lockTarget == null)
            {
                RushDistance = 10f;
            }
            else
            {
                RushDistance = 9999999f;
                LockData.smooth = 9999F;
            }
        }




        ///////////////////////////////////////////////////////////////////////////////////////////////




        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("attack1-1(2)"))
        {
            RushDistance = 3f;
            cha.stopmove = 0;
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("attack1-2(3)"))
        {
            if (Input.GetMouseButton(1))
            {
                anim.SetBool("attack2", true);
            }

        }


        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("attack2-1(2)"))
        {
            RushDistance = 3f;
            cha.stopmove = 0;
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("attack2-2(3)"))
        {
            if (Input.GetMouseButton(1))
            {
                anim.SetBool("attack3", true);
            }
        }



        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("attack3-1(1)"))
        {
            RushDistance = 4.5f;
            cha.stopmove = 0;
            anim.SetTrigger("attackover");
        }


        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("start_2_vmd"))
        {
            blade.SetActive(false);
            shield.SetActive(false);
            gun.SetActive(true);
            cha.stopmove = 0;
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("start_1-2_vmd"))
        {
            cha.stopmove = 0;
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("endpose"))
        {
            LockData.smooth = 8;
        }




        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("overpose"))
        {
            RushDistance = 0f;
            PlayerDamageResist = 1;
            blade.SetActive(false);
            shield.SetActive(false);
            FireBlade.SetActive(false);
            gun.SetActive(true);
            bulletcontroller.fire = true;
            anim.SetBool("defense", false);
            anim.SetBool("counter", false);
            anim.SetBool("attack2", false);
            anim.SetBool("attack3", false);
            anim.SetBool("FireAttack_2", false);
            anim.SetBool("FireAttack_3", false);
            anim.SetBool("FireAttack_4", false);
            anim.SetBool("DuelAttack", false);
            anim.SetBool("Duel", false);
            gunactive.gunactive = true;
            cha.stopmove = 1;
            LockData.radius = 0.1f;
            LockData.smooth = 15;
            anim.SetBool("counter", false);
            SPWeapon.SetActive(false);
            cha.power = false;
            


        }
        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("defense2"))
        {
            PlayerDamageResist = 0;
            if (cha.losehp == true)
            {
                anim.SetBool("counter", true);
            }
        }


        //else if (anim.GetCurrentAnimatorStateInfo(0).IsName("leftroll3"))
        //{
        //    cha.stopmove = 1;            
        //}

        //else if (anim.GetCurrentAnimatorStateInfo(0).IsName("leftroll3"))
        //{
        //    cha.stopmove = 1;            
        //}

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("rightroll5"))
        {
            PlayerDamageResist = 0;
            transform.Translate(0.03f, 0, 0);
            FireBlade.SetActive(false);
            bulletcontroller.fire = false;
            blade.SetActive(false);
            shield.SetActive(false);
            gun.SetActive(true);
            bullet.SetActive(true);
            cha.stopmove = 0;
            LockData.smooth = 10;
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("leftroll4"))
        {
            PlayerDamageResist = 0;
            transform.Translate(-0.03f, 0, 0);
            FireBlade.SetActive(false);
            bulletcontroller.fire = false;
            blade.SetActive(false);
            shield.SetActive(false);
            gun.SetActive(true);
            bullet.SetActive(true);
            cha.stopmove = 0;
            LockData.smooth = 10;
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("specialshoot_vmd"))
        {
            cha.stopmove = 0;
            bulletcontroller.fire = false;
            gunactive.gunactive = false;
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("LaserSkill"))
        {
            gunactive.gunactive = false;
            blade.SetActive(false);
            shield.SetActive(false);
            gun.SetActive(false);
            bulletcontroller.fire = false;
            //bullet.SetActive(false);
            //cha.stopmove = 0.3f;
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("MissileSkill"))
        {
            gunactive.gunactive = false;
            blade.SetActive(false);
            shield.SetActive(false);
            gun.SetActive(false);
            bulletcontroller.fire = false;
            //bullet.SetActive(false);
            //cha.stopmove = 0.3f;
        }

        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("MissileSkill"))
        {
            gunactive.gunactive = false;
            blade.SetActive(false);
            shield.SetActive(false);
            gun.SetActive(false);
            bulletcontroller.fire = false;
            //bullet.SetActive(false);
            //cha.stopmove = 0.3f;
        }





        else if (anim.GetCurrentAnimatorStateInfo(0).IsName("lose_1"))
        {
            JumpNum.SetActive(false);

            gunactive.gunactive = false;
            if (ami.Awakening == false) 
            { 
                blade.SetActive(true); 
            }

            if (ami.Awakening == true)
            {
                FireBlade.SetActive(true);
            }


            shield.SetActive(true);
            gun.SetActive(false);
            bulletcontroller.fire = false;
            cha.stopmove = 0;
            anim.SetBool("shoot", false);
            if (Boss_1_UI != null)
            {
                Boss_1_UI.SetActive(false);
            }


            if (FinalBoss_UI != null)
            {
                FinalBoss_UI.SetActive(false);
            }
       
        
        }

    }

    void Return()
    {
        SceneManager.LoadScene(0);
    }
    void FireBlade_1() 
    {
        AS.PlayOneShot(FireBladeAttackVoice);
        AS.PlayOneShot(FireBladeAttackSlashSoundEff);
        Destroy(Instantiate(FireBladeSlash_1, FireBladeBox_1.transform.position, FireBladeBox_1.transform.rotation), 1);
        if (PowerUpTime > 0) 
        {
            Destroy(Instantiate(FireBladeFire_1, FireBox_1.transform.position, FireBox_1.transform.rotation), 1);
            Destroy(Instantiate(RedPartical_1, FireBox_1.transform.position, FireBox_1.transform.rotation), 1);

            Destroy(Instantiate(FireBladeSlash_Wave_1, FireBladeWaveBox.transform.position, FireBladeWaveBox.transform.rotation), 1);
        }
    }

    void FireBlade_2()
    {
        AS.PlayOneShot(FireBladeAttackSlashSoundEff);
        Destroy(Instantiate(FireBladeSlash_2, FireBladeBox_2.transform.position, FireBladeBox_2.transform.rotation), 1);
        if (PowerUpTime > 0)
        {
            Destroy(Instantiate(FireBladeFire_1, FireBox_2.transform.position, FireBox_2.transform.rotation), 1);
            Destroy(Instantiate(RedPartical_2, FireBox_1.transform.position, FireBox_1.transform.rotation), 1);

            Destroy(Instantiate(FireBladeSlash_Wave_2, FireBladeWaveBox.transform.position, FireBladeWaveBox.transform.rotation), 1);
        }
    }

    void FireBlade_3()
    {
        AS.PlayOneShot(FireBladeAttackSlashSoundEff);
        Destroy(Instantiate(FireBladeSlash_3, FireBladeBox_3.transform.position, FireBladeBox_3.transform.rotation), 1);
        if (PowerUpTime > 0)
        {
            Destroy(Instantiate(FireBladeFire_1, FireBox_3.transform.position, FireBox_3.transform.rotation), 1);
            Destroy(Instantiate(RedPartical_3, FireBox_1.transform.position, FireBox_1.transform.rotation), 1);
            Destroy(Instantiate(FireBladeSlash_Wave_3, FireBladeWaveBox.transform.position, FireBladeWaveBox.transform.rotation), 1);
        }
    }

    void FireBlade_4()
    {
        AS.PlayOneShot(FireBladeAttackSlashSoundEff);
        Destroy(Instantiate(FireBladeSlash_4, FireBladeBox_4.transform.position, FireBladeBox_4.transform.rotation), 1);
        if (PowerUpTime > 0)
        {
            Destroy(Instantiate(FireBladeFire_1, FireBox_4.transform.position, FireBox_4.transform.rotation), 1);
            Destroy(Instantiate(RedPartical_4, FireBox_1.transform.position, FireBox_1.transform.rotation), 1);
            Destroy(Instantiate(FireBladeSlash_Wave_4, FireBladeBox_4.transform.position, FireBladeBox_4.transform.rotation), 1);
        }
    }

    void Duel() 
    {
        AS.PlayOneShot(PowerUpSoundEff);
        AS.PlayOneShot(PowerUpVoice);
        PowerUpTime = 15;
        Destroy(Instantiate(PowerUp, PowerUpBox.transform.position, PowerUpBox.transform.rotation), 1);
    }

    void DuelAttack_() 
    {
        Destroy(Instantiate(DuelAttack, DuelAttackBox.transform.position, DuelAttackBox.transform.rotation), 1);
        AS.PlayOneShot(DuelAttackVoice);
        if (PowerUpTime > 0)
        {
            Destroy(Instantiate(DuelAttackWave, FireBladeBox_4.transform.position, FireBladeBox_4.transform.rotation), 5);
        }
    }




    void Slash_1() 
    {
        Destroy(Instantiate(Slash1, HitBox1.transform.position, HitBox1.transform.rotation), 1);
    }
    void Slash_2()
    {
        Destroy(Instantiate(Slash2, HitBox2.transform.position, HitBox2.transform.rotation), 0.6f);
    }
    void Slash_3()
    {
        Destroy(Instantiate(Slash3, HitBox3.transform.position, HitBox3.transform.rotation), 0.7f);
    }

    void Counter_1() 
    {
        Destroy(Instantiate(counterslash_1_1, counter1.transform.position, counter1.transform.rotation), 0.8f);
        Destroy(Instantiate(counterslash_1_2, counter1.transform.position, counter1.transform.rotation), 0.8f);
        Destroy(Instantiate(counterslash_1_3, counter1.transform.position, counter1.transform.rotation), 0.8f);
    }

    void Counter_2()
    {
        Destroy(Instantiate(counterslash_2_1, counter2.transform.position, counter2.transform.rotation), 0.8f);
        Destroy(Instantiate(counterslash_2_2, counter2.transform.position, counter2.transform.rotation), 0.8f);
    }

    void Counter_Bird()
    {
        Destroy(Instantiate(counterslash_Bird, counterBird.transform.position, counterBird.transform.rotation), 0.8f);
    }

    void SpecialShoot_1() 
    {
        Destroy(Instantiate(SpecialShootReadyPrefab, SpecialShootBox.transform.position, SpecialShootBox.transform.rotation), 0.8f);
    }

    void SpecialShoot_2()
    {
        //Destroy(Instantiate(SpecialShootmuzzlePrefab_1, SpecialShootBox.transform.position, SpecialShootBox.transform.rotation), 0.8f);
        Destroy(Instantiate(SpecialShootmuzzlePrefab_2, SpecialShootBox.transform.position, SpecialShootBox.transform.rotation), 0.8f);
    }

    void Laser_1()
    {
        Destroy(Instantiate(LaserReady, LaserBox.transform.position, LaserBox.transform.rotation), 0.8f);
        Destroy(Instantiate(LaserWave, LaserBox.transform.position, LaserBox.transform.rotation), 0.8f);
        Destroy(Instantiate(LaserShoot, LaserBox.transform.position, LaserBox.transform.rotation), 0.8f);
    }

    void Laser_2()
    {
        Destroy(Instantiate(LaserReady, LaserBox2.transform.position, LaserBox2.transform.rotation), 0.8f);
        Destroy(Instantiate(LaserShoot, LaserBox2.transform.position, LaserBox2.transform.rotation), 0.8f);
        Destroy(Instantiate(LaserWave, LaserBox2.transform.position, LaserBox2.transform.rotation), 0.8f);
    }

    void SP_1()
    {
        //Destroy(Instantiate(SPMuzzle1, SpBox1.transform.position, SpBox1.transform.rotation), 0.05f);
        SPMuzzle1.SetActive(true);
    }

    void SP_2()
    {
        Destroy(Instantiate(SPMuzzle2, SpBox2.transform.position, SpBox2.transform.rotation), 0.3f);
        Destroy(Instantiate(SPShoot, SpBox2.transform.position, SpBox2.transform.rotation), 0.3f);
        Destroy(Instantiate(SPWave, SpBox2.transform.position, SpBox2.transform.rotation), 0.3f);
        SPMuzzle1.SetActive(false);
    }


    void shack() 
    {
        MyInpulse.GenerateImpulse();
    }

    void FireBladeRush() 
    {
        
        transform.Translate(0, 0, RealRush * movedis);
    }

    void BladeRush_1() 
    {
        Debug.Log("第一次");
        transform.Translate(0, 0, RealRush * movedis);
    }

    void BladeRush_2()
    {
        transform.Translate(0, 0, RealRush * movedis);
    }

    void BladeRush_3()
    {
        transform.Translate(0, 0, RealRush * movedis);
    }

    void CounterRush_1() 
    {
        RushDistance = 14f;
    }
    void CounterRush_2()
    {     
        transform.Translate(0, 0, RealRush * movedis);
    }
   
}

