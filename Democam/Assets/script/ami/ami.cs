﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Playables;

public class ami : MonoBehaviour
{

    private Animator anim;
    public bool gunactive;
    public GameObject bullet;
    public GameObject canon1;
    public GameObject canon2;
    public CharacterController cha;
    public amistate amistate;

    public GameObject locks;
    public GameObject[] targets;
    public float SearchRadius;
    public GameObject missiles;
    public bool Awakening;




    private float missileCD;
    public Image missileCDUI;
    
    private float laserCD;
    public Image laserCDUI;

    public static float SPpower;
    private float SP;
    public Image SPUI;


    public Image PowerUpUI;


    public PlayableDirector SPTimeLine;
    AudioSource AS;
    public AudioClip SPReady;
    public AudioClip SPReadyVoice;
    private bool SPOKEff;
    private float SPSoundTime;





    public int a;
    void Start()
    {
        cha = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        amistate = GameObject.Find("一条綾香").GetComponent<amistate>();
        anim = gameObject.GetComponent<Animator>();
        SP = 120;
        AS = GetComponent<AudioSource>();

        Awakening = false;
    }

    void missile() 
    {
       
        var missiles = bullet.GetComponent<missile>();
        var lo = locks.GetComponent<LockOn>();
        print("fire");
        if (lo.targets != null)
        {
            missiles.target = lo.targets;
            Instantiate(bullet, canon1.transform.position, canon1.transform.rotation);
            Instantiate(bullet, canon2.transform.position, canon2.transform.rotation);
        }
        else
        {
            Instantiate(bullet, canon1.transform.position, canon1.transform.rotation);
            Instantiate(bullet, canon2.transform.position, canon2.transform.rotation);
        }
    }

    void OnTriggerEnter(Collider DamageEvent)
    {
        if (DamageEvent.tag == "EnemyAttack")
        {
            anim.SetBool("counter",true);
        }
    }

    void OnTriggerExit(Collider DamageEvent)
    {
        if (DamageEvent.tag == "EnemyAttack")
        {
            anim.SetBool("counter", false);
        }
    }


  




    void Update()
    {
        if (amistate.PowerUpTime > 0) 
        {
            amistate.PowerUpTime -= Time.deltaTime;
            if (amistate.PowerUpTime < 0)
            {
                amistate.PowerUpTime = 0;
            }

            PowerUpUI.fillAmount = amistate.PowerUpTime / 15;

        }



        if (missileCD >= 0)
        {
            missileCD -= Time.deltaTime;
            if (missileCD < 0) 
            {
                missileCD = 0;
            }
            //print(missileCD);

            missileCDUI.fillAmount = missileCD / 10;
        }
        //飛彈CD判斷+UI顯示

        if (laserCD >= 0)
        {
            laserCD -= Time.deltaTime;
            if (laserCD < 0)
            {
                laserCD = 0;
            }

            laserCDUI.fillAmount = laserCD / 15;
        }

        ////////////////////////////////////////////////////////////////

        if (SPOKEff == true&& SPSoundTime == 1)
        {
            AS.PlayOneShot(SPReady);
            AS.PlayOneShot(SPReadyVoice);
            SPOKEff = false;
            SPSoundTime = 0;
        }

        if (SPpower == 0) 
        {
            SPOKEff = false;
            SPSoundTime = 1;
        }

        if (cha.PlayerHp > 0)
        {
            if (SPpower / SP >= 1)
            {
                SPOKEff = true;
                SPpower = 120;

                if (Input.GetKeyDown("r"))
                {
                    SPTimeLine.Play();
                    anim.SetBool("SP", true);
                    SPpower = 0;
                }
            }
        }

        SPUI.fillAmount = SPpower / SP;

        


        //////////////////////////////////////////////////////////////




        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");


       

        if (Input.GetKey(KeyCode.LeftShift))
        {
            anim.SetFloat("speed", v*2f);
            anim.SetFloat("direction", h*2f);
        }
        else 
        {
            anim.SetFloat("speed", v);
            anim.SetFloat("direction", h);
        }

        if (Input.GetKeyDown("c"))
        {
            if (Awakening == false)
            {
                anim.SetTrigger("defense(0)");
                anim.SetBool("defense", true);
            }
            if (Awakening == true) 
            {
                anim.SetBool("Duel", true);
            }
        }



        if (laserCD == 0)
        {
            if (Input.GetKeyDown("q"))
            {
                anim.SetBool("Laser", true);
            }
            else
            {
                anim.SetBool("Laser", false);
            }
        }

        if (MainController.GameState > 1)
        {

            if (missileCD == 0)
            {
                if (Input.GetKeyDown("e"))
                {
                    anim.SetBool("Missile", true);
                }
                else
                {
                    anim.SetBool("Missile", false);
                }
            }
        }







        if (Input.GetMouseButtonDown(1) && Awakening == false)
        {
            anim.SetBool("attack", true);
        }
        else
        {
            anim.SetBool("attack", false);
        }

        if (Input.GetMouseButtonDown(1) && Awakening == true) 
        {
            anim.SetBool("FireAttack_1", true);
        }
        else
        {
            anim.SetBool("FireAttack_1", false);
        }


        if (Input.GetMouseButton(1))
        {
            
            anim.SetBool("attackready", true);
        }
        else
        {
            anim.SetBool("attackready", false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            anim.SetBool("roll", true);
        }
        else
        {
            anim.SetBool("roll", false);
        }

        if (cha.PlayerHp <= 0)
        {
            anim.SetBool("lose", true);
        }
        else 
        {
            anim.SetBool("lose", false);
        }

       


        if (gunactive == true)
        {
            if (Input.GetMouseButton(0))
            {

                anim.SetBool("shoot", true);
            }
            else
            {
                anim.SetBool("shoot", false);
            }
        }

        //if (cha.curhp <= 0)
        //{
        //    anim.SetBool("Dead", true);
        //}
        //else 
        //{
        //    anim.SetBool("Dead", false);
        //}
    }


    void missileColdDown() 
    {
        missileCD = 10f;
    }


    void laserColdDown()
    {
        laserCD = 15f;
    }

    void SPOver()
    {
        anim.SetBool("SP", false);
    }

}
   
