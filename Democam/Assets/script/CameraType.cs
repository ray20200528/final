﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraType : MonoBehaviour
{
    public GameObject startcam, maincam;
    public GameObject startobj, mainobj;
    public bool Battle;

    void Start()
    {
        maincam.SetActive(false);
        mainobj.SetActive(false);



        startcam.SetActive(true);
        startobj.SetActive(true);
        Battle = false;
    }
    void BattleType() 
    {
        Battle = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Battle == false) 
        {
            maincam.SetActive(false);
            mainobj.SetActive(false);


            startcam.SetActive(true);
            startobj.SetActive(true);
        }

        if (Battle == true) 
        {
            maincam.SetActive(true);
            mainobj.SetActive(true);


            startobj.SetActive(false);
            startcam.SetActive(false);
        }
    }
}
