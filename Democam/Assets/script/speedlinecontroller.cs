﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

[RequireComponent(typeof(ParticleSystem))]
public class speedlinecontroller : MonoBehaviour
{
    
    private float h;
    private float v;

    //public PaticleSystem ps;
    // Start is called before the first frame update
    void Start()
    {
        ParticleSystem ps = GetComponent<ParticleSystem>();
        ps.startSpeed = (h + v) * 100;

    }

    // Update is called once per frame
    void Update()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");

        if (v < 0 || h < 0)
        {
            h = h * -1;
            v = v * -1;
        }
       
    }
}
