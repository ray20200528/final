﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UImain : MonoBehaviour
{
    public Object floatingBar;
    public Object floatingText;
    public Dropdown dp;
    private List<FloatingBar> fbs = new List<FloatingBar>();
    public static UImain m_Instance;
    public RectTransform rootUI;
    public Canvas mainCanvas;
    public Camera uiCam;
    public Object playerText;
    private void Awake()
    {
        m_Instance = this;
        test(Vector3.one);
    }

   

    void test(Vector2 vv)
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        rootUI = GetComponent<RectTransform>();
        mainCanvas = GetComponent<Canvas>();
    }

    // Update is called once per frame
   

    
    public void SpawnFloatingText(Vector3 pos, string sText)
    {
        GameObject g = Instantiate(floatingText) as GameObject;
        g.transform.SetParent(this.transform);
        g.transform.localScale = Vector3.one;
        FloatingText f = g.GetComponent<FloatingText>();
        f.Spawn(uiCam, pos, sText);

    }
    public void SpawnPlayerText(Vector3 pos, string sText)
    {
        GameObject g = Instantiate(playerText) as GameObject;
        g.transform.SetParent(this.transform);
        g.transform.localScale = Vector3.one;
        FloatingText f = g.GetComponent<FloatingText>();
        f.Spawn(uiCam, pos, sText);

    }




}
