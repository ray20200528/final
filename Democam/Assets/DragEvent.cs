﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragEvent : MonoBehaviour
{
    public Transform rootCanvas;
    private Image image;
    public Transform oriParent;
    private Vector3 oriLocalPosition;
    private Vector3 oriOffset;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
    }

    public void OnBeginDrag()
    {
        oriParent = image.transform.parent;
        oriLocalPosition = image.transform.localPosition;
        oriOffset = image.transform.position - Input.mousePosition;
        image.transform.SetParent(rootCanvas);
        image.raycastTarget = false;
        UIgear.m_Instance.setcurrentDrag(this);
    }

    public void ExchangeParent(Transform t)
    {
        this.transform.SetParent(t);
        image.transform.localPosition = oriLocalPosition;
    }

    public void OnDrag()
    {
        image.transform.position = Input.mousePosition + oriOffset;
    }

    public void Drop(Transform target)
    {
        //Transform tChild = target.transform.GetChild(0);
        //if(tChild != null)
        //{
        //    // DragEvent de = tChild.GetComponent<DragEvent>();
        //    // de.ExchangeParent(oriParent);
        //    tChild.SendMessage("ExchangeParent", oriParent);
        //}
        this.transform.SetParent(target);
    }

    public void DropItem()
    {
        DragEvent de = UIgear.m_Instance.currentDrag();
        de.Drop(this.transform.parent);
        ExchangeParent(de.oriParent);
       
    }

    public void OnEndDrag()
    {
        if (image.transform.parent == rootCanvas)
        {
            image.transform.SetParent(oriParent);
        }
        image.transform.localPosition = oriLocalPosition;
        image.raycastTarget = true;
        Debug.Log("OnEndDrag");
    }
}
