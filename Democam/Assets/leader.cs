﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leader : MonoBehaviour
{
    enum FSM
    {
        None = -1,
        Combat,
        chase,
        shoot,
        back,
        knife,
        idle,
        wander,
        win,
        soon,
        hurt

    }
    CharacterController chr;
    private float distanceToMe;           
    public GameObject target;                           
    private Animator ani;
    private FSM state;
    private float Idletime;
    private float currenttime;
    public int r;
    public float rotspeed;
    public GameObject bullet;
    public float Timer;
    public float radius;
    private SkinnedMeshRenderer sm;
    public GameObject bomb;
    public GameObject boom;
    public GameObject gun;
    public GameObject blade;
    public GameObject slash;
    public GameObject door;
    EnemyHP enemyhp;
    public AudioClip hurt;
    AudioSource ads;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindWithTag("Player");
        ani = GetComponent<Animator>();
        //curhp = maxhp;
        state = FSM.idle;
        currenttime = 0.0f;
        Idletime = Random.Range(3.0f, 4.0f);
        Timer = 2;
        r = Random.Range(0, 4);
        sm = GetComponentInChildren<SkinnedMeshRenderer>();
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        enemyhp = GetComponent<EnemyHP>();
        ads = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 md = target.transform.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(md);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
        if (ani.GetCurrentAnimatorStateInfo(0).IsName("ene1_100_anim_d03"))
        {
            ani.SetBool("shoot", false);
        }
        if (chr.PlayerHp > 0)
        {
            if (chr.power != true)
            {
                if (state == FSM.idle)
                {
                    ani.SetBool("woof", true);
                    //sm.enabled = false;
                    //transform.Translate(100, 0, 0);

                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        state = FSM.soon;
                        ani.SetBool("woof", false);
                        Idletime = Random.Range(1, 2);
                        Destroy(Instantiate(door, transform.position, Quaternion.identity), 0.8f);
                        Invoke("Soon", 0.5f);

                        //transform.position = target.transform.position + new Vector3(0, 0, -10);
                        //sm.enabled = true;
                        //rend.enabled = true;
                        //r = Random.Range(0, 2);
                        //switch (r)
                        //{
                        //    case 0:
                        //        state = FSM.knife;
                        //        break;
                        //    case 1:
                        //        state = FSM.Combat;
                        //        break;
                        //    case 2:
                        //        state = FSM.shoot;
                        //        ani.SetBool("shoot", true);
                        //        break;
                        //        //case 3:
                        //        //    transform.Translate(-0.1f, -0.05f, -0.1f);
                        //        //    break;
                        //        //case 4:
                        //        //    transform.Translate(-0.1f, 0.1f, -0.1f);
                        //        //    break;
                        //}
                    }
                    else
                    {

                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.chase)
                {
                    //ani.SetTrigger("chase");
                    distanceToMe = Vector3.Distance(target.transform.position, this.transform.position);
                    this.transform.Translate(Vector3.forward * 0.2f);  //向目标前进，即靠近（Vector3.back 后退，则逃避）
                    if (distanceToMe <= 3f)
                    {
                        this.transform.Translate(Vector3.forward * -0.2f);
                        state = FSM.knife;
                        Idletime = 2;
                    }
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        state = FSM.Combat;
                        Idletime = 1.5f;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }

                }
                else if (state == FSM.Combat)
                {
                    transform.LookAt(target.transform);
                    ani.SetBool("turnkill", true);
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        state = FSM.back;
                        r = Random.Range(0, 4);
                        ani.SetBool("turnkill", false);
                        ani.SetBool("back", true);
                        Idletime = 2f;
                        //Invoke("Bomb", 2);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.knife)
                {
                    ani.SetBool("blade", true);
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        state = FSM.Combat;
                        
                        ani.SetBool("blade", false);
                        Idletime = 3;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.wander)
                {


                    transform.Translate(20*Time.deltaTime, 0, 0);


                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        Idletime = Random.Range(2.0f, 3.0f);
                        state = FSM.chase;
                        r = Random.Range(0, 2);
                        switch (r)
                        {
                            case 0:
                                state = FSM.chase;
                                break;
                            case 1:
                                state = FSM.Combat;
                                break;
                            case 2:
                                state = FSM.shoot;
                                Idletime = 4;
                                break;
                                //case 3:
                                //    transform.Translate(-0.1f, -0.05f, -0.1f);
                                //    break;
                                //case 4:
                                //    transform.Translate(-0.1f, 0.1f, -0.1f);
                                //    break;
                        }
                    }
                    else
                    {

                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.back)
                {

                    switch (r)
                    {
                        case 0:
                            transform.Translate(10 * Time.deltaTime, 0, -10 * Time.deltaTime);
                            break;
                        case 1:
                            transform.Translate(-10 * Time.deltaTime, 0, -10 * Time.deltaTime);
                            break;
                        case 2:
                            transform.Translate(0, 0, -10 * Time.deltaTime);
                            break;
                        case 3:
                            transform.Translate(-10 * Time.deltaTime, 0, -10 * Time.deltaTime);
                            break;
                        case 4:
                            transform.Translate(-10 * Time.deltaTime, 0, -10 * Time.deltaTime);
                            break;
                    }
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        state = FSM.shoot;
                        Idletime = 4;
                        ani.SetBool("back", false);
                        r = Random.Range(0, 3);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.shoot)
                {

                    ani.SetBool("shoot", true);
                    gun.transform.LookAt(target.transform.position);
                    if (currenttime > Idletime)
                    {
                        //ani.SetBool("shoot", false);
                        currenttime = 0.0f;
                        switch (r)
                        {
                            case 0:
                                state = FSM.wander;
                                break;
                            case 1:
                                state = FSM.idle;
                                Idletime = 3;
                                break;
                            case 2:
                                state = FSM.chase;
                                break;
                            case 3:
                                state = FSM.Combat;
                                break;

                        }

                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.soon)
                {

                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;

                        Idletime = 4;
                        Destroy(Instantiate(door, target.transform.localPosition + new Vector3(0, 0, -10), Quaternion.identity), 0.8f);
                        Invoke("Soonback", 0.5f);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
            }else
            {
                state = FSM.hurt;
                CancelInvoke();
                sm.enabled = true;
                    
            }
        }
        else
        {
            state = FSM.win;
            CancelInvoke();
            sm.enabled = true;
        }
        if (state == FSM.win)
        {
            ani.SetBool("win",true);
            CancelInvoke();
            if(chr.PlayerHp>0)
            {
                ani.SetBool("win", false);
                state = FSM.idle;
            }
        }
        if(state == FSM.hurt)
        {
            if (chr.power)
            {
                state = FSM.idle;
            }
        }
        if(enemyhp.hp<=0)
        {
            ani.SetBool("dead", true);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerAttack")
        {
            ads.PlayOneShot(hurt);
        }
    }
    void Soon()
    {
        sm.enabled = false;
    }
    void Soonback()
    {
        sm.enabled = true;
        transform.position = target.transform.localPosition + new Vector3(0, 0, -10);
        state = FSM.chase;
    }

    void Dead()
    {
        Destroy(Instantiate(boom, transform.position, Quaternion.identity), 0.8f);
        this.gameObject.SetActive(false);

        //GameObject.Find("enemypool").GetComponent<enemypool>().Recovery(gameObject);
    }
    void Shoot()
    {
        Instantiate(bullet, gun.transform.position, gun.transform.rotation);
    }
    void knife()
    {
        //Instantiate(slash, transform.position,transform.rotation);
        Vector3 bulletDir = this.transform.position;
        Quaternion leftRota = Quaternion.AngleAxis(-20, Vector3.up);
        Quaternion RightRota = Quaternion.AngleAxis(20, Vector3.up);
        for (int i = 0; i < 1; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                switch (j)
                {
                    case 0:
                        Instantiate(slash, transform.position, transform.rotation);
                        break;
                    case 1:
                        bulletDir = RightRota * bulletDir;
                        Instantiate(slash, transform.position, RightRota * transform.rotation);
                        break;
                    case 2:
                        bulletDir = leftRota * (leftRota * bulletDir);
                        Instantiate(slash, transform.position, leftRota * transform.rotation);
                        bulletDir = RightRota * bulletDir;
                        break;
                }
            }

        }
    }
    void Bomb()
    {
        Instantiate(bomb, transform.position, transform.rotation);
    }
}
