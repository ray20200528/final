﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bomb : MonoBehaviour
{
    public float timer;
    public GameObject bullet;
    public Transform player;
    public float life;
    public float distanceToMe;

    // Start is called before the first frame update
    void Start()
    {
        timer = 2.0f;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        distanceToMe = Vector3.Distance(player.transform.position, this.transform.position);       
        transform.Translate(0, 0, 0.2f);
        life -= Time.deltaTime;
        timer -= Time.deltaTime;
        if (timer <= 1)
        {
            transform.Translate(0, 0, -0.2f);
        }
        if (timer <= 0)
        {
            //StartCoroutine(FanBarrage(37));
            exp();
            timer = 5;
        }
    }
    IEnumerator FanBarrage(int num)
    {
        while (true)
        {
            Vector3 bulletDir = Vector3.up;
            float offset = num % 2 != 0 ? 10 : 5;
            float firstOffset = (num % 2) == 0 ? offset : 0;
            int left = (num % 2) == 0 ? -1 : 1;
            for (int i = 0; i < num; i++)
            {

                Quaternion dir = Quaternion.AngleAxis(left * firstOffset, bulletDir);

                Instantiate(bullet, transform.position, dir * transform.rotation);

                if (left > 0)
                {
                    firstOffset += offset;
                }
                left = -left;
            }
            yield return new WaitForSeconds(1f);
        }
    }
    IEnumerator Spiral()
    {
        //子弹发射初始方向
        Vector3 bulletDir = Vector3.up;
        //螺旋次数
        int SpiraCount = 18;
        //角度偏移量
        float offset = 720 / SpiraCount;
        //初始弹幕爆炸时间
        //float boomTime = 0.002f;
        ////时间增量
        //float boomTimeIncrement = 0.0005f;
        //螺旋时间间隔
        float time = 0f;

        for (int i = 0; i < SpiraCount; i++)
        {
            //计算旋转后的方向
            Quaternion dir = Quaternion.AngleAxis(i * offset, bulletDir);
            //GameObject go = ObjectPool.Instance.GetObj(ShootConst.BULLETNAMES[bulletID]);
            ////设置子弹位置
            //go.transform.position = firPosition;
            Instantiate(bullet, transform.position, dir * this.transform.rotation);
            //设置子弹旋转
            //fanBarrage.qua = dir;
            ////开火
            //fanBarrage.Fire(num);
            //暂停
            yield return new WaitForSeconds(time);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            exp();
        }
    }
    public void exp()
    {
        //StartCoroutine(FanBarrage(37));
        StartCoroutine(Spiral());
        Invoke("dead", 0.1f);
    }
    public void dead()
    {
        Destroy(gameObject);
    }
}
