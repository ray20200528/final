﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class floatgun : MonoBehaviour
{
    public enum FSM
    {
        None = -1,
        Idle,
        Run,
        Attack,
        Wander,
        Back

    }
    private FSM state;
    private float Idletime;
    private float currenttime;
    public float sight;
    public float attackrange;
    public float timer;
    public GameObject bullet;
    public GameObject player;
    public int r;
    public Animator ani;
    public float curhp;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        timer = 1.0f;
        ani = GetComponent<Animator>();
        state = FSM.Attack;
        currenttime = 0.0f;
        Idletime = Random.Range(0.5f, 0.8f);
        attackrange = Vector3.Distance(player.transform.position, transform.position);
        curhp = 100;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 md = player.transform.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(md);
        transform.rotation = rot;
        //transform.LookAt(m_Target.transform);
        Debug.Log("Current State " + state);
        if (state == FSM.Idle)
        {

            if (attackrange < 2f)
            {
                transform.Translate(0.8f, -0.4f, -0.2f);
            }
            else
            {
                transform.Translate(0.8f, -0.4f, 0.2f);
            }
            // Check Dead
            if (currenttime > Idletime)
            {
                currenttime = 0.0f;
                state = FSM.Wander;
            }
            else
            {
                currenttime += Time.deltaTime;
            }
        }
        else if (state == FSM.Wander)
        {
            transform.Translate(-0.8f, 0.4f, -0.2f);
            if (currenttime > Idletime)
            {
                currenttime = 0.0f;
                state = FSM.Attack;
            }
            else
            {
                currenttime += Time.deltaTime;
            }


        }
        else if (state == FSM.Run)
        {
            if (attackrange < 2f)
            {
                transform.Translate(0.8f, 0.3f, -0.2f);
            }
            else
            {
                transform.Translate(0.8f, 0.3f, 0.2f);
            }
            //transform.Translate(0.8f, 0.3f, 0.2f);

            if (currenttime > Idletime)
            {
                currenttime = 0.0f;
                state = FSM.Attack;
            }
            else
            {
                currenttime += Time.deltaTime;
            }


        }
        else if (state == FSM.Attack)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {

                ani.SetTrigger("attack");
                Instantiate(bullet, transform.position, transform.rotation);
                timer = 1.0f;
            }
            if (currenttime > Idletime)
            {
                currenttime = 0.0f;
                r = Random.Range(1, 4);
                switch (r)
                {
                    case 0:
                        state = FSM.Idle;
                        break;
                    case 1:
                        state = FSM.Run;
                        break;
                    case 2:
                        //state = FSM.Wander;
                        break;
                    case 3:
                        state = FSM.Back;
                        break;
                }
                //state = FSM.Wander;
            }
            else
            {
                currenttime += Time.deltaTime;
            }


        }
        else if (state == FSM.Back)
        {
            transform.Translate(0.8f, -0.3f, -0.2f);
            if (currenttime > Idletime)
            {
                currenttime = 0.0f;
                state = FSM.Attack;
            }
            else
            {
                currenttime += Time.deltaTime;
            }
        }
        //Flee();
        //Move();

    }
    void Hpstate(int hps)
    {

        curhp += hps;
        if (curhp <= 0)
        {
            this.gameObject.SetActive(false);
            print("You Win!!!");
        }

    }
    void OnTriggerEnter(Collider bu)
    {
        print("shoot");
        if (bu.tag == "bullet")
        {
            Hpstate(-50);

        }
    }


    private void OnDrawGizmos()
    {


        Debug.DrawLine(this.transform.position, this.transform.position + transform.forward * 30.0f, Color.red);
        //if (state == FSM.Idle)
        //{
        //    Gizmos.color = Color.green;
        //}
        //else if (state == FSM.Wander)
        //{
        //    Gizmos.color = Color.blue;
        //    Gizmos.DrawLine(this.transform.position, targets);
        //}
        //else if (state == FSM.Run)
        //{
        //    Gizmos.color = Color.yellow;
        //    Gizmos.DrawLine(this.transform.position, targets);
        //}
        //else if (state == FSM.Attack)
        //{
        //    Gizmos.color = Color.red;
        //}
        //else if (state == FSM.Back)
        //{
        //    Gizmos.color = Color.gray;
        //}
        //Gizmos.DrawWireSphere(this.transform.position, sight);

        //Gizmos.color = Color.red;
        //Gizmos.DrawWireSphere(this.transform.position, m_fProbeLength);
    }
}
