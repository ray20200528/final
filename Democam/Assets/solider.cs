﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class solider : MonoBehaviour
{
    enum FSM
    {
        None = -1,
        Combat,
        chase,
        shoot,
        back,
        win,
        idle,
        hurt
    }
    private float distanceToMe;           //智能体到目标的距离
    public GameObject target;                //目标角色     
    private Animator ani;
    private FSM state;
    private float Idletime;
    private float currenttime;
    public int r;
    public float rotspeed;
    public GameObject bullet;
    public float Timer;
    public GameObject blade;
    EnemyHP enemyhp;
    public GameObject knife;
    CharacterController chr;
    public GameObject exp;
    public AudioClip hurt;
    AudioSource ads;
    void Start()
    {
        target = GameObject.FindWithTag("Player");
        ani = GetComponent<Animator>();
        state = FSM.idle;
        currenttime = 0.0f;
        Idletime = Random.Range(2.0f, 3.0f);
        Timer = 1;
        r = Random.Range(0, 4);
        blade.SetActive(false);
        //blood = GetComponentInChildren<Image>();
        enemyhp = GetComponent<EnemyHP>();
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        ads = GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update()
    {
        if (ani.GetCurrentAnimatorStateInfo(0).IsName("ene1_021_anim_c02"))
        {
            ani.SetBool("fire", false);
        }
        Vector3 md = target.transform.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(md);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
        Debug.Log("Current State " + state);
        if (chr.PlayerHp > 0)
        {
            if (chr.power != true)
            {
                if (state == FSM.chase)
                {
                    distanceToMe = Vector3.Distance(target.transform.position, this.transform.position);
                    //if (currenttime > Idletime)
                    //{
                    //    currenttime = 0.0f;
                    //    state = FSM.shoot;
                    //    Idletime = Random.Range(1.0f, 2.0f);
                    //}
                    //else
                    //{
                    //    currenttime += Time.deltaTime;
                    //}
                    if (distanceToMe < 500)
                    {
                        //transform.rotation = Quaternion.Lerp(transform.rotation, rot,rotspeed);
                        //this.transform.LookAt(target.transform);               //该方法使智能体总是面对目标
                        this.transform.Translate(Vector3.forward * 0.05f);  //向目标前进，即靠近（Vector3.back 后退，则逃避）

                        if (distanceToMe <= 3f)
                        {
                            this.transform.Translate(Vector3.forward * -0.05f);

                            ani.SetBool("bladeattack", true);

                            if (ani.GetCurrentAnimatorStateInfo(0).IsName("blade_attack_03"))
                            {
                                blade.SetActive(false);
                                state = FSM.back;
                                ani.SetBool("bladeattack", false);
                                r = Random.Range(0, 4);
                                Idletime = Random.Range(1, 1.5f);
                            }
                        }

                    }

                }
                else if (state == FSM.back)
                {

                    switch (r)
                    {
                        case 0:
                            transform.Translate(10 * Time.deltaTime, 0, -10 * Time.deltaTime);
                            break;
                        case 1:
                            transform.Translate(-10 * Time.deltaTime, 0, -10 * Time.deltaTime);
                            break;
                        case 2:
                            transform.Translate(0, 0, -0.05f);
                            break;
                        case 3:
                            transform.Translate(-10 * Time.deltaTime, 0.5f*Time.deltaTime, -10 * Time.deltaTime);
                            break;
                        case 4:
                            transform.Translate(-10 * Time.deltaTime, -0.5f * Time.deltaTime, -10 * Time.deltaTime);
                            break;
                    }
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        state = FSM.shoot;
                        Idletime = Random.Range(1.0f, 2.0f);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.shoot)
                {
                    ani.SetBool("fire", true);

                    //if (Timer > 0)
                    //{

                    //    Timer -= Time.deltaTime;

                    //}
                    //else if (Timer <= 0)
                    //{
                    //    ani.SetBool("fire", true);
                    //    Instantiate(bullet, transform.position, transform.rotation);
                    //    Timer = 2.0f;
                    //}
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        state = FSM.chase;
                        Idletime = Random.Range(2.0f, 3.0f);
                    }
                    else
                    {

                        currenttime += Time.deltaTime;
                    }

                }
                else if (state == FSM.idle)
                {
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        state = FSM.chase;
                        Idletime = Random.Range(2.0f, 3.0f);
                    }
                    else
                    {

                        currenttime += Time.deltaTime;
                    }
                }
            }else
            {
                state = FSM.hurt;
                ani.SetBool("fire", false);
                ani.SetBool("bladeattack", false);
            }
        }
        else
        {
            state = FSM.win;
        }
        if (state == FSM.win)
        {
            ani.SetBool("vic", true);
            if (chr.PlayerHp > 0)
            {
                Idletime = Random.Range(2.0f, 3.0f);
                state = FSM.idle;
                ani.SetBool("vic", false);
            }
        }
        if(state == FSM.hurt)
        {
            if (chr.power)
            {
                state = FSM.idle;
            }
        }
        if (enemyhp.hp <= 0)
        {
            ani.SetBool("dead", true);
           
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerAttack")
        {
            ads.PlayOneShot(hurt);
        }
    }
    void shoot()
    {
        Instantiate(bullet, transform.position, transform.rotation);
    }
    private void OnDrawGizmos()
    {
        Debug.DrawLine(this.transform.position, this.transform.position + transform.forward * 20.0f, Color.red);
    }
    
    
    void Dead()
    {
        //Destroy(Instantiate(exp, transform.position, Quaternion.identity), 0.5f); 
        Destroy(Instantiate(exp, transform.position, Quaternion.identity), 0.8f);
        this.gameObject.SetActive(false);
        //Destroy(gameObject);
        //GameObject.Find("enemypool").GetComponent<enemypool>().Recovery(gameObject);
    }
    void bladed()
    {
        blade.SetActive(true);
        Destroy(Instantiate(knife, blade.transform.position,blade.transform.rotation), 0.8f);
    }
}
