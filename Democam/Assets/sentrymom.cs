﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class sentrymom : MonoBehaviour
{
    enum FSM
    {
        idle,
        shoot,
        move,
        win,
        birth,
        walk,
        hurt
    }
    public Transform player;
    public float Timer;
    public GameObject bullet;
    public float rotspeed;
    EnemyHP enemyhp;
    CharacterController chr;
    public GameObject exp;
    private FSM state;
    private float Idletime;
    private float currenttime;
    public int r;
    public GameObject son;
    public GameObject door;
    public GameObject laserbeam;
    public int count;
    public AudioClip hurt;
    AudioSource ads;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        Timer = 2.0f;
        enemyhp = GetComponent<EnemyHP>();
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        state = FSM.birth;
        currenttime = 0.0f;
        //Idletime = Random.Range(1, 2);
        Idletime = 1;
        ads = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 md = player.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(md);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
        if (chr.PlayerHp > 0)
        {
            if (chr.power != true)
            {
                if (state == FSM.birth)
                {
                    //Destroy(Instantiate(door, transform.position + new Vector3(-5, 0, 0), Quaternion.identity), 0.1f);
                    //Destroy(Instantiate(door, transform.position + new Vector3(5, 0, 0), Quaternion.identity), 0.1f);
                    Destroy(Instantiate(door, transform.position + new Vector3(0, 5, 0), Quaternion.identity), 0.1f);
                    Destroy(Instantiate(door, transform.position + new Vector3(0, -5, 0), Quaternion.identity), 0.1f);
                    if (currenttime > Idletime)
                    {
                        //Instantiate(son, transform.position + new Vector3(-5, 0, 0), Quaternion.identity);
                        //Instantiate(son, transform.position + new Vector3(5, 0, 0), Quaternion.identity);
                        Instantiate(son, transform.position + new Vector3(0, 5, 0), Quaternion.identity);
                        Instantiate(son, transform.position + new Vector3(0, -5, 0), Quaternion.identity);
                        //InvokeRepeating("birth", 1, 2f);
                        currenttime = 0.0f;
                        r = Random.Range(0, 2);
                        switch (r)
                        {
                            case 0:
                                state = FSM.move;
                                Idletime = Random.Range(1, 2);
                                break;
                            case 1:
                                state = FSM.idle;
                                Idletime = Random.Range(1, 2);
                                break;
                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.shoot)
                {
                    //Instantiate(bullet, transform.position, transform.rotation);
                    if (currenttime > Idletime)
                    {
                        Destroy(Instantiate(bullet, transform.position, transform.rotation), 0.5f);
                        Instantiate(laserbeam, transform.position, transform.rotation);
                        currenttime = 0.0f;
                        state = FSM.idle;


                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.move)
                {
                    switch (r)
                    {
                        case 0:
                            transform.Translate(0.03f, 0, 0);
                            break;
                        case 1:
                            transform.Translate(-0.03f, 0, 0);
                            break;
                        case 2:
                            transform.Translate(0.05f, 0.05f, 0);
                            break;

                    }
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        r = Random.Range(0, 2);
                        switch (r)
                        {
                            case 0:
                                state = FSM.birth;
                                Idletime = 1;
                                //r = Random.Range(0, 2);
                                break;
                            case 1:
                                state = FSM.shoot;
                                break;
                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.idle)
                {
                    if (currenttime > Idletime)
                    {
                        r = Random.Range(0, 2);
                        currenttime = 0.0f;
                        state = FSM.move;
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
            }else
            {
                state = FSM.hurt;
            }
            
        }else
        {
            state = FSM.win;
            
        }
        if(enemyhp.hp<=0)
        {
            Destroy(Instantiate(exp, transform.position, Quaternion.identity), 1);
            this.gameObject.SetActive(false);
            //Destroy(son);
            //Destroy(gameObject);
        }
        if (state == FSM.win)
        {
            if (chr.PlayerHp > 0)
            {
                currenttime = 0.0f;
                state = FSM.idle;

            }
        }
        if(state == FSM.hurt)
        {
            if (chr.power)
            {
                state = FSM.idle;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerAttack")
        {
            ads.PlayOneShot(hurt);
        }
    }
    public void birth()
    {
        Destroy(Instantiate(door, transform.position + new Vector3(0,5, 0), transform.rotation), 0.5f);
        Destroy(Instantiate(door, transform.position + new Vector3(0,-5, 0), transform.rotation), 0.5f);
        //Destroy(Instantiate(door, transform.position + new Vector3(5, -5, 0), transform.rotation), 0.5f);
        //Destroy(Instantiate(door, transform.position + new Vector3(-5, -5, 0), transform.rotation), 0.5f);
        Invoke("secondbirth", 0.5f);
        count += 1;
        if (count == 5)
        {
            CancelInvoke();
        }
    }
    public void secondbirth()
    {
        Instantiate(son, transform.position + new Vector3(0, 5, 0), transform.rotation);
        Instantiate(son, transform.position + new Vector3(0, -5, 0), transform.rotation);
    }
}
