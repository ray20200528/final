﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flaotcanon : MonoBehaviour
{
    enum FSM
    {
        idle,
        shoot,
        move,
        win,
        up,
        down,
        hurt
    }
    public Transform player;
    public float Timer;
    public GameObject bullet;
    public float rotspeed;
    EnemyHP enemyhp;
    CharacterController chr;
    public GameObject exp;
    private FSM state;
    private float Idletime;
    private float currenttime;
    public int r;
    public AudioClip hurt;
    AudioSource ads;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        Timer = 2.0f;
        enemyhp = GetComponent<EnemyHP>();
        chr = GameObject.Find("一条綾香").GetComponent<CharacterController>();
        state = FSM.up;
        currenttime = 0.0f;
        Idletime = Random.Range(1, 2);
        ads = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 md = player.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(md);
        transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotspeed);
        if (chr.PlayerHp > 0)
        {
            if (chr.power != true)
            {
                if (state == FSM.up)
                {
                    transform.Translate(0, 0.01f, 0);
                    if (Timer > 0)
                    {

                        Timer -= Time.deltaTime;

                    }
                    else if (Timer <= 0)
                    {
                        if (chr.PlayerHp > 0)
                        {
                            Instantiate(bullet, transform.position, transform.rotation);
                            Timer = 2.0f;
                        }
                        else
                        {
                            Timer += 10 * Time.deltaTime;
                        }
                    }
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        state = FSM.down;

                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.shoot)
                {
                    if (currenttime > Idletime)
                    {
                        Instantiate(bullet, transform.position, transform.rotation);

                        currenttime = 0.0f;
                        r = Random.Range(0, 2);
                        switch (r)
                        {
                            case 0:
                                state = FSM.move;
                                r = Random.Range(0, 2);
                                Idletime = Random.Range(1, 2);
                                break;
                            case 1:
                                state = FSM.up;
                                break;
                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.move)
                {
                    switch (r)
                    {
                        case 0:
                            transform.Translate(0.01f, 0, 0);
                            break;
                        case 1:
                            transform.Translate(-0.01f, 0, 0);
                            break;
                        case 2:
                            transform.Translate(0.01f, 0.01f, 0);
                            break;

                    }
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        switch (r)
                        {
                            case 0:
                                state = FSM.up;
                                r = Random.Range(0, 2);
                                Idletime = Random.Range(1, 2);
                                break;
                            case 1:
                                state = FSM.shoot;
                                Idletime = Random.Range(1, 2);
                                break;
                        }
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                else if (state == FSM.down)
                {
                    transform.Translate(0, -0.01f, 0);
                    if (Timer > 0)
                    {

                        Timer -= Time.deltaTime;

                    }
                    else if (Timer <= 0)
                    {
                        if (chr.PlayerHp > 0)
                        {
                            Instantiate(bullet, transform.position, transform.rotation);
                            Timer = 2.0f;
                        }
                        else
                        {
                            Timer += 10 * Time.deltaTime;
                        }
                    }
                    if (currenttime > Idletime)
                    {
                        currenttime = 0.0f;
                        state = FSM.move;
                        Idletime = Random.Range(1.0f, 2.0f);
                        r = Random.Range(0, 2);
                    }
                    else
                    {
                        currenttime += Time.deltaTime;
                    }
                }
                
            }else
            {
                state = FSM.hurt;
            }
        }
        else
        {
            state = FSM.win;

        }
        if(state == FSM.hurt)
        {
            if (chr.power)
            {
                state = FSM.shoot;
            }
        }
        if (state == FSM.win)
        {
            if (chr.PlayerHp > 0)
            {
                currenttime = 0.0f;
                state = FSM.move;
                Idletime = Random.Range(1.0f, 2.0f);
                r = Random.Range(0, 2);
            }
        }
        if (enemyhp.hp <= 0)
        {
            Destroy(Instantiate(exp, transform.position, Quaternion.identity), 1);
            this.gameObject.SetActive(false);
            //Destroy(gameObject);
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerAttack")
        {
            ads.PlayOneShot(hurt);
        }
    }
}
