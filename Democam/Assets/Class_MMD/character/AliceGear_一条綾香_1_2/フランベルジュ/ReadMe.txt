■はじめに
　ダウンロードして頂きありがとうございます！

　アリス・ギア・アイギスよりフランベルジュ
　(C)Pyramid,Inc/Colopl,Inc

　データ製作：RGM


■注意事項
　・モデルデータの商用利用と無改造での再配布は禁止です。
　・改造、及び流用したデータの配布はOKです。
　・このデータを使用したことによって発生したあらゆる損害に対し、こちらでは責任を負いかねます。
　・常に最新版の利用規約がすべてのバージョンに適用されます。

■モデルについて
　・AutoLuminousで刀身部分が光ります。モーフで強度調整。
　
　なにかあったらニコニコ辺りに投げてもらえれば出来る限り反応します。
　このデータが、少しでも隊長達のお役に立てることを願って
　RGM　http://www.nicovideo.jp/user/18491

　
　履歴：
　2020/02/15 v1.0 配布

