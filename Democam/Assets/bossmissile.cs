﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossmissile : MonoBehaviour
{
    public float speed = 12;
    public float rotspeed = 60;
    public GameObject exp;
    public Transform target;
    public float acc = 20;
    public float timer;
    // Start is called before the first frame update
    void Start()
    {

        timer = 3;
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {

        if (target != null)
        {
            Vector3 tarv = (target.position - this.transform.position).normalized;
            float a = Vector3.Angle(transform.forward, tarv) / rotspeed;
            if (a > 0.1f || a < -0.1f)
            {
                transform.forward = Vector3.Slerp(transform.forward, tarv, Time.deltaTime / a).normalized;
            }
            else
            {
                speed += acc * Time.deltaTime;
                transform.forward = Vector3.Slerp(transform.forward, tarv, 1).normalized;
            }
            transform.position += transform.forward * speed * Time.deltaTime;

        }
        else
        {
            transform.position += transform.forward * 10 * Time.deltaTime;


        }

        if (timer <= 0)
        {
            Destroy(gameObject);
            //Destroy(other.gameObject);
            Destroy(Instantiate(exp, transform.position, Quaternion.identity), 0.8f);
        }
        timer -= Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(gameObject);
            //Destroy(other.gameObject);
            Destroy(Instantiate(exp, transform.position, Quaternion.identity), 0.8f);
            //blood.fillAmount -= 0.2f;dsa
        }
    }

}
