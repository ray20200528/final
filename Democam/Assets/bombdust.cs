﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bombdust : MonoBehaviour
{
    public float timer;
    // Start is called before the first frame update
    void Start()
    {
        timer = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (timer <= 0) Destroy(gameObject);
        timer -= Time.deltaTime;
        
    }
}
